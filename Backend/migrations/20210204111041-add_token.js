const { Sequelize, DataTypes } = require('sequelize');
const { tokenTypes } = require('../src/config/tokens');


'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.createTable('token', {
      token: {
        type: DataTypes.STRING(500),
        allowNull: false,
        primaryKey: true
      },
      user: {
        type: Sequelize.INTEGER,
        references: {
          model: 'praticiens',
          key: 'id'
        },
        allowNull: false
      },
      type: {
        type: Sequelize.ENUM(tokenTypes.REFRESH, tokenTypes.RESET_PASSWORD),
        allowNull: false
      },
      expires: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      blacklisted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
    });

  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('token');
  }
};

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('abonnements', {
    /* id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    }, */
    idpraticien: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'praticiens',
        key: 'id'
      }
    },
    datedebut: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    datefin: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'abonnements',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "abonnements_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};

const Sequelize = require('sequelize');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('clients', {
    /*id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },*/
    email: {
      type: DataTypes.STRING(50),
      allowNull: true,
      unique: "clients_email_key"
    },
    nom: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    prenom: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'clients',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "clients_email_key",
        unique: true,
        fields: [
          { name: "email" },
        ]
      },
      {
        name: "clients_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('consultations', {
    /* id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    }, */
    idpraticien: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'praticiens',
        key: 'id'
      }
    },
    idclient: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'clients',
        key: 'id'
      }
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    duree: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'consultations',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "consultations_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};

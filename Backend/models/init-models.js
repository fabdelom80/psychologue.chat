var DataTypes = require("sequelize").DataTypes;
var _clients = require("./clients");
var _consultations = require("./consultations");
var _praticiens = require("./praticiens");

function initModels(sequelize) {
  var clients = _clients(sequelize, DataTypes);
  var consultations = _consultations(sequelize, DataTypes);
  var praticiens = _praticiens(sequelize, DataTypes);

  //relation
  consultations.belongsTo(clients, { as: "idclient_client", foreignKey: "idclient"});
  clients.hasMany(consultations, { as: "consultations", foreignKey: "idclient"});
  consultations.belongsTo(praticiens, { as: "idpraticien_praticien", foreignKey: "idpraticien"});
  praticiens.hasMany(consultations, { as: "consultations", foreignKey: "idpraticien"});

  return {
    clients,
    consultations,
    praticiens,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;

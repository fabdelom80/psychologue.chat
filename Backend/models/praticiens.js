const Sequelize = require('sequelize');
const bcrypt = require('bcryptjs');

module.exports = function(sequelize, DataTypes) {
  const Praticien = sequelize.define('praticiens', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "praticiens_email_key"
    },
    nom: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    prenom: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    mdp: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    tel: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    adresse: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    siret: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "praticiens_siret_key"
    },
    adeli: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: "praticiens_adeli_key"
    },
    valid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    online: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    role: {
      type: DataTypes.STRING(50),
      defaultValue: 'pra'
    }
  }, {
    sequelize,
    tableName: 'praticiens',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "praticiens_adeli_key",
        unique: true,
        fields: [
          { name: "adeli" },
        ]
      },
      {
        name: "praticiens_email_key",
        unique: true,
        fields: [
          { name: "email" },
        ]
      },
      {
        name: "praticiens_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "praticiens_siret_key",
        unique: true,
        fields: [
          { name: "siret" },
        ]
      },
    ]
  });

  Praticien.prototype.isPasswordMatch = function(password) {
    return bcrypt.compare(password, this.mdp);
  }
  Praticien.prototype.toJSON = function () {
    var values = Object.assign({}, this.get());
    delete values.mdp;
    return values;
  }

  return Praticien;
};



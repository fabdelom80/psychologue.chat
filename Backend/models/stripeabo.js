const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('stripeabo', {

    idstripe: {
      type: DataTypes.STRING(320),
      allowNull: true,
    },
    valid: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(320),
      allowNull: true,
    }

  }, {
    sequelize,
    tableName: 'stripeabo',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "praticiens_idstripe_key",
        unique: true,
        fields: [
          { name: "idstripe" },
        ]
      },
    ]
  });
};

const Sequelize = require('sequelize');
const { tokenTypes } = require('../src/config/tokens');


module.exports = function (sequelize, DataTypes) {
    const Token = sequelize.define('token', {

        token: {
            type: DataTypes.STRING(500),
            allowNull: false,
            primaryKey: true
        },
        user: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Praticien',
                key: 'id'
            },
            allowNull: false
        },
        type: {
            type: Sequelize.ENUM(tokenTypes.REFRESH, tokenTypes.RESET_PASSWORD),
            allowNull: false
        },
        expires: {
            type: Sequelize.DATE,
            allowNull: true,
        },
        blacklisted: {
            type: Boolean,
            defaultValue: false,
        },


    }, {
        sequelize,
        tableName: 'token',
        schema: 'public',
        timestamps: false
    });

    return Token;
};



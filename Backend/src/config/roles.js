const roles = ['pra', 'admin','pra-admin'];

const roleRights = new Map();
roleRights.set(roles[0], ['pra']);
roleRights.set(roles[1], ['admin']);
roleRights.set(roles[2], ['pra', 'admin']);

module.exports = {
  roles,
  roleRights,
};

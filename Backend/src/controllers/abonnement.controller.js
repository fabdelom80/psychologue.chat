const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { abonnementService, praticienService } = require('../services');


const createAbonnement = catchAsync(async (req, res) => {
  let dateDebutAbo = await abonnementService.getDateEndAboByPraticien(req.body.idPraticien);
  let dateFinAbo = new Date(dateDebutAbo);
  let moisSupp = req.body.month;
  dateFinAbo = dateFinAbo.setMonth(dateFinAbo.getMonth()+moisSupp);
  let amount = req.body.amount * 100;
  try {
    let customer = await stripe.customers.create({
      email: req.body.email,
      card: req.body.id
    });
    let charges = await stripe.charges.create({
      amount,
      description: "Abonnement Thérapeute",
         currency: "eur",
         customer: customer.id
    });
    if (charges.status=="succeeded") {
      await abonnementService.createAbonnement({
        "idpraticien": req.body.idPraticien,
        "datedebut": dateDebutAbo,
        "datefin": dateFinAbo
      });
      res.json({message: "OK"});
    }
    else {res.json({message: "KO"});}
  } catch (error) {
    res.json({message: error.raw.code});}
});



const getAbonnements = catchAsync(async (req, res) => {
  const abonnement = await abonnementService.getAbonnements();
  res.send(abonnement);
});

const getAbonnement = catchAsync(async (req, res) => {
  const abonnement = await abonnementService.getAbonnementById(req.params.id);
  if (!abonnement) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Abonnement not found');
  }
  res.send(abonnement);
});

const createTempAbo1Month = catchAsync(async (req, res) => {
  const result = await praticienService.getPraticien(req.params.id);
  email= result.dataValues.email;
  product= 'prod_ItDv7YSWXPLaFE';
  amount= 1000;
  session = await abonnementService.createTempAbo(email, product, amount);
  res.json(session.id);
});

const createTempAbo1Year = catchAsync(async (req, res) => {
  const result = await praticienService.getPraticien(req.params.id);
  email= result.dataValues.email;
  product= 'prod_IvdM5oRTQDhSjE';
  amount= 10000;
  session = await abonnementService.createTempAbo(email, product, amount);
  res.json(session.id);
});

const createTempAboFullLife = catchAsync(async (req, res) => {
  const result = await praticienService.getPraticien(req.params.id);
  email= result.dataValues.email;
  product= 'prod_IvdOe7vDuyieoO';
  amount= 55000;
  session = await abonnementService.createTempAbo(email, product, amount);
  res.json(session.id);
});


const updateAbonnement = catchAsync(async (req, res) => {
  const abonnement = await abonnementService.updateAbonnementById(req.params.id, req.body);
  res.send(abonnement);
});

const deleteAbonnement = catchAsync(async (req, res) => {
  await abonnementService.deleteAbonnementById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});



const getAbonnementByPraticien = catchAsync(async (req, res) => {
  const abonnement = await abonnementService.getAbonnementByPraticien(req.params.idpraticien);
  if (!abonnement) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Abonnement not found');
  }
  res.send(abonnement);
});

const getLastAbonnementByPraticien = catchAsync(async (req, res) => {
  const abonnement = await abonnementService.getLastAbonnementByPraticien(req.params.idpraticien);
  if (!abonnement) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Abonnement not found');
  }
  res.send(abonnement);
});

module.exports = {
  getAbonnements,
  createAbonnement,
  getAbonnement,
  updateAbonnement,
  deleteAbonnement,
  getAbonnementByPraticien,
  getLastAbonnementByPraticien,
  createTempAbo1Month,
  createTempAboFullLife,
  createTempAbo1Year
};

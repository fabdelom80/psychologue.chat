const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { administrateurService } = require('../services');

const createAdministrateur = catchAsync(async (req, res) => {
  const administrateur = await administrateurService.createAdministrateur(req.body);
  res.status(httpStatus.CREATED).send(administrateur);
});

const getAdministrateurs = catchAsync(async (req, res) => {
  const result = await administrateurService.getAdministrateurs();
  res.send(result);
});

const getAdministrateur = catchAsync(async (req, res) => {
  const administrateur = await administrateurService.getAdministrateurById(req.params.id);
  if (!administrateur) {
    throw new ApiError(httpStatus.NOT_FOUND, 'administrateur not found');
  }
  res.send(administrateur);
});

const getAdministrateurByEmail = catchAsync(async (req, res) => {
  const administrateur = await administrateurService.getAdministrateurByEmail(req.params.email);
  if (!administrateur) {
    throw new ApiError(httpStatus.NOT_FOUND, 'administrateur not found');
  }
  res.send(administrateur);
});

const updateAdministrateur = catchAsync(async (req, res) => {
  const administrateur = await administrateurService.updateAdministrateurById(req.params.id, req.body);
  res.send(administrateur);
});

const deleteAdministrateur = catchAsync(async (req, res) => {
  await administrateurService.deleteAdministrateurById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  getAdministrateurs,
  createAdministrateur,
  getAdministrateur,
  updateAdministrateur,
  deleteAdministrateur,
  getAdministrateurByEmail,
};

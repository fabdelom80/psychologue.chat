const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { clientService } = require('../services');


const getClients = catchAsync(async (req,res) => {
  const result = await clientService.queryClients();
  res.send(result);
});

const getClient = catchAsync(async (req,res) => {
  const result = await clientService.queryClient(req.params.id);
  res.send(result);
});

const createClient = catchAsync(async (req,res) => {
  const result = await clientService.createClient(req.body);
  res.send(result);
});

const deleteClient = catchAsync(async (req, res) => {
  await clientService.deleteClient(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  getClients,
  getClient,
  createClient,
  deleteClient
};

const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { consultationService } = require('../services');


const createConsultation = catchAsync(async (req, res) => {
  const consultation = await consultationService.createConsultation(req.body);
  res.status(httpStatus.CREATED).send(consultation);
});

const getConsultations = catchAsync(async (req, res) => {
  const result2 = await consultationService.getConsultations();
  res.send(result2);
});

const getConsultation = catchAsync(async (req, res) => {
  const consultation = await consultationService.getConsultationById(req.params.id);
  if (!consultation) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Consultation not found');
  }
  res.send(consultation);
});


const updateConsultation = catchAsync(async (req, res) => {
  const consultation = await consultationService.updateConsultationById(req.params.id, req.body);
  res.send(consultation);
});

const deleteConsultation = catchAsync(async (req, res) => {
  await consultationService.deleteConsultationById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

const getConsultationByClient = catchAsync(async (req, res) => {
  const consultation = await consultationService.getConsultationByClient(req.params.idclient);
  if (!consultation) {
    throw new ApiError(httpStatus.NOT_FOUND, 'consultation not found');
  }
  res.send(consultation);
});

const getConsultationByPraticien = catchAsync(async (req, res) => {
  const consultation = await consultationService.getConsultationByPraticien(req.params.idpraticien);
  if (!consultation) {
    throw new ApiError(httpStatus.NOT_FOUND, 'consultation not found');
  }
  res.send(consultation);
});

module.exports = {
  getConsultations,
  createConsultation,
  getConsultation,
  updateConsultation,
  deleteConsultation,
  getConsultationByClient,
  getConsultationByPraticien,
};

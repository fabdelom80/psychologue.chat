module.exports.authController = require('./auth.controller');
module.exports.abonnementController = require('./abonnement.controller');
module.exports.consultationController = require('./consultation.controller');
module.exports.webhookController = require('./webhook.controller');

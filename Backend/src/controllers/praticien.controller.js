const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { praticienService } = require('../services');

const getPraticiens = catchAsync(async (req,res) => {
  const result = await praticienService.getPraticiens();
  res.send(result);
});

const getPraticien = catchAsync(async (req,res) => {
  const result = await praticienService.getPraticien(req.params.id);
  res.send(result);
});

const getPraticienByEmail = catchAsync(async (req,res) => {
  const result = await praticienService.getPraticienByEmail(req.params.email);
  res.send(result);
});

const getBooleanPraticienByEmail = catchAsync(async (req,res) => {
  const result = await praticienService.getBooleanPraticienByEmail(req.params.email);
  res.send(result);
});

const getPraticienByAdeli = catchAsync(async (req,res) => {
  const result = await praticienService.getPraticienByAdeli(req.params.adeli);
  res.send(result);
});

const getPraticienBySiret = catchAsync(async (req,res) => {
  const result = await praticienService.getPraticienBySiret(req.params.siret);
  res.send(result);
});

const getPraticiensByValidite = catchAsync(async (req,res) => {
  const result = await praticienService.getPraticiensByValidite(req.params.value);
  res.send(result);
});

const getPraticiensAvailable = catchAsync(async (req,res) => {
  const result = await praticienService.getPraticiensAvailable(req.query);
  res.send(result);
});

// const createPraticien = catchAsync(async (req,res) => {
//   const result = await praticienService.createPraticien(req.body);
//   res.send(result);
// });

const updatePraticien = catchAsync(async (req, res) => {
  const result = await praticienService.updatePraticienById(req.params.id, req.body);
  res.send(result);
});

const deletePatricien = catchAsync(async (req, res) => {
  await praticienService.deletePraticien(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
    getPraticiens,
    getPraticien,
    getPraticiensByValidite,
    getPraticiensAvailable,
    // createPraticien,
    updatePraticien,
    deletePatricien,
    getPraticienByEmail,
    getPraticienByAdeli,
    getPraticienBySiret,
    getBooleanPraticienByEmail
  };
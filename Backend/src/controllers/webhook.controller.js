const { praticienService, abonnementService, stripeaboService } = require('../services');
const stripe = require('stripe')(process.env.StripekeySecret);
const endpointSecret = process.env.endpointSecret;
const createWebhook = async (request,response) => {
  const payload = request.body;
    const sig = request.headers['stripe-signature'];
    let event;
    try {
      event = stripe.webhooks.constructEvent(payload, sig, endpointSecret);
    } catch (err) {
      return response.status(400).send(`Webhook Error: ${err.message}`);
    }
    switch (event.type) {
      case 'checkout.session.completed': {
        const session = event.data.object;
        const stripeabo = await stripeaboService.createStripeabo({
          idstripe: session.id,
          valid: false,
          email: session.customer_email
        });
        // ici je créer le paiement en attente avec l'id session.id + session.customer_email
        if (session.payment_status === 'paid') {
          const stripeabo = await stripeaboService.getStripeaboById(session.id);
          if(stripeabo) {
            stripeaboService.updateStripeaboById(session.id, {
              valid: true
            });
            praticien = await praticienService.getPraticienByEmail(session.customer_email);
            idPraticien = praticien.id;
            month = stripeaboService.getMonthAbo(session.amount_total);
            abonnementService.createAbonnement(idPraticien, month);
          }
        }
        break;
      }
      case 'checkout.session.async_payment_succeeded': {
        const session = event.data.object;
        const stripeabo = await stripeaboService.getStripeaboById(session.id);
        if(stripeabo) {
          stripeaboService.updateStripeaboById(session.id, {
            valid: true
          });
          praticien = await praticienService.getPraticienByEmail(session.customer_email);
          idPraticien = praticien.id;
          month = stripeaboService.getMonthAbo(session.amount_total);
          abonnementService.createAbonnement(idPraticien, month);
        }
        break;
      }
  
      case 'checkout.session.async_payment_failed': {
        const session = event.data.object;
        // Envoie d'email si paiement echoue ? 
        break;
      }
    }
    response.status(200).send("OK");
};



module.exports = {
  createWebhook,
  };
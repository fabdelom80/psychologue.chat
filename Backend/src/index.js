const app = require('./app');
const config = require('./config/config');
const logger = require('./config/logger');
const { Sequelize } = require('sequelize');

server = app.listen(config.port, () => {
  logger.info(`Listening to port ${config.port}`);
});

const sequelize = new Sequelize(config.postgres_uri) // Example for postgres

sequelize.authenticate().then(()=> console.log('Connection to pg has been established successfully.'))
.catch((error)=>console.error('Unable to connect to the Postgres database:', error))
 


const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});

const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');

const { abonnementValidation } = require('../../validations');
const { abonnementController } = require('../../controllers');

const router = express.Router();


router
  .route('/')
  .post(auth('pra'), validate(abonnementValidation.createAbonnement), abonnementController.createAbonnement)
  .get(auth('pra'), validate(abonnementValidation.getAbonnements), abonnementController.getAbonnements);

router
  .route('/:id')
  .get(auth('pra'), validate(abonnementValidation.getAbonnement), abonnementController.getAbonnement)
  .patch(auth('pra'), validate(abonnementValidation.updateAbonnement), abonnementController.updateAbonnement)
  .delete(auth('pra'), validate(abonnementValidation.deleteAbonnement), abonnementController.deleteAbonnement);

router
  .route('/last/praticien/:idpraticien')
  .get(auth('pra'), validate(abonnementValidation.getLastAbonnementByPraticien), abonnementController.getLastAbonnementByPraticien)

  
router
  .route('/praticien/:idpraticien')
  .get(auth('pra'), validate(abonnementValidation.getAbonnementByPraticien), abonnementController.getAbonnementByPraticien)

module.exports = router;


/**
 * @swagger
 * tags:
 *   name: Abonnements
 *   description: Abonnement management and retrieval
 */

/**
 * @swagger
 * path:
 *  /abonnements:
 *    get:
 *      summary: Get all abonnements from database
 *      description: 
 *      tags: [Abonnements]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  datedebut:
 *                    type: string
 *                  datefin:
 *                    type: string
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                datedebut: 2020-01-01
 *                datefin: 2020-02-01
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 * 
 *    post:
 *      summary: Create an abonnement
 *      description: Abonnement for praticiens.
 *      tags: [Abonnements]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - idpraticien
 *                - datedebut
 *                - datefin
 *              properties:
 *                idpraticien:
 *                  type: integer
 *                datedebut:
 *                  type: string
 *                datefin:
 *                  type: string 
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  datedebut:
 *                    type: string
 *                  datefin:
 *                    type: string
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * path:
 *  /abonnements/{id}:
 *    get:
 *      summary: Get an abonnement
 *      description: To check the id before deleting.
 *      tags: [Abonnements]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Abonnement id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  datedebut:
 *                    type: string
 *                  datefin:
 *                    type: string
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                datedebut: 2020-01-01
 *                datefin: 2020-02-01
 *    delete:
 *      summary: Delete a Abonnement
 *      description: Delete by id
 *      tags: [Abonnements]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Abonnement id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *    patch:
 *      summary: Update an Abonnement
 *      description: To update an Abonnement.
 *      tags: [Abonnements]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Abonnement id
 *      requestBody:
 *        required : true
 *        content:
 *          application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  idpraticien:
 *                    type: integer
 *                  datedebut:
 *                    type: string
 *                  datefin:
 *                    type: string
 *              example:
 *                idpraticien: 1
 *                datedebut: 2020-01-01
 *                datefin: 2020-03-01
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  datedebut:
 *                    type: string
 *                  datefin:
 *                    type: string
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                datedebut: 2020-01-01
 *                datefin: 2020-03-01
 */

 /**
 * @swagger
 * path:
 *  /abonnements/praticien/{idpraticien}:
 *    get:
 *      summary: Get a Abonnements by praticien
 *      description: To check Abonnement's praticien.
 *      tags: [Abonnements]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: idpraticien
 *          required: true
 *          schema:
 *            type: string
 *          description: ID praticien
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  datedebut:
 *                    type: string
 *                  datefin:
 *                    type: string
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                datedebut: 2020-01-01
 *                datefin: 2020-02-01
 */


 /**
 * @swagger
 * path:
 *  /abonnements/last/praticien/{idpraticien}:
 *    get:
 *      summary: Get a last Abonnement by praticien
 *      description: To check the last Abonnement's praticien.
 *      tags: [Abonnements]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: idpraticien
 *          required: true
 *          schema:
 *            type: string
 *          description: ID praticien
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  datedebut:
 *                    type: string
 *                  datefin:
 *                    type: string
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                datedebut: 2020-01-01
 *                datefin: 2020-02-01
 */
const express = require('express');
//const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const clientValidation = require('../../validations/clients.validation');
const clientController = require('../../controllers/clients.controller');

const router = express.Router();

router
  .route('/')
  .get(validate(clientValidation.getClients), clientController.getClients)
  .post(validate(clientValidation.createClient), clientController.createClient);

router
  .route('/:id')
  .get(/*auth('getClient'),*/ validate(clientValidation.getClient), clientController.getClient)
  .delete(/*auth('manageClients'),*/ validate(clientValidation.deleteClient), clientController.deleteClient);


/**
 * @swagger
 * tags:
 *   name: Client
 *   description: Client management and retrieval
 */

/**
 * @swagger
 * path:
 *  /clients:
 *    get:
 *      summary: Get all clients from database
 *      description: Only admins can retrieve all clients.
 *      tags: [Client]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: email
 *          schema:
 *            type: string
 *          description: Client email
 *        - in: query
 *          name: nom
 *          schema:
 *            type: string
 *          description: Client nom
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *              example:
 *                id: 999
 *                email: fake@example.com
 *                nom: fakeNom
 *                prenom : fakePrenom
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 * 
 *    post:
 *      summary: Create a client
 *      description: Client is created to participate in a consultation.
 *      tags: [Client]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - email
 *                - nom
 *                - prenom
 *              properties:
 *                email:
 *                  type: string
 *                  format: email
 *                nom:
 *                  type: string 
 *                prenom:
 *                  type: string
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * path:
 *  /clients/{id}:
 *    get:
 *      summary: Get a client
 *      description: To check the id before deleting.
 *      tags: [Client]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Client id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *              example:
 *                id: 999
 *                email: fake@example.com
 *                nom: fakeNom
 *                prenom : fakePrenom
 *    delete:
 *      summary: Delete a client
 *      description: Delete by id
 *      tags: [Client]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Client id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

module.exports = router;

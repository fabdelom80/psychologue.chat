const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');

const { consultationValidation } = require('../../validations');
const { consultationController } = require('../../controllers');

const router = express.Router();


router
  .route('/')
  .post(auth('pra'), validate(consultationValidation.createConsultation), consultationController.createConsultation)
  .get(auth('pra'), validate(consultationValidation.getConsultations), consultationController.getConsultations);

router
  .route('/:id')
  .get(auth('pra'), validate(consultationValidation.getConsultation), consultationController.getConsultation)
  .patch(auth('pra'), validate(consultationValidation.updateConsultation), consultationController.updateConsultation)
  .delete(auth('admin'), validate(consultationValidation.deleteConsultation), consultationController.deleteConsultation);

// pas utile tout de suite ... 
router
  .route('/client/:idclient')
  .get(auth('pra'), validate(consultationValidation.getConsultationByClient), consultationController.getConsultationByClient)
  
router
  .route('/praticien/:idpraticien')
  .get(auth('pra'), validate(consultationValidation.getConsultationByPraticien), consultationController.getConsultationByPraticien)

module.exports = router;


/**
 * @swagger
 * tags:
 *   name: Consultations
 *   description: Consultation management and retrieval
 */

/**
 * @swagger
 * path:
 *  /consultations:
 *    get:
 *      summary: Get all consultations from database
 *      description: 
 *      tags: [Consultations]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  idclient:
 *                    type: integer
 *                  date:
 *                    type: string
 *                  duree:
 *                    type: integer
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                idclient: 2
 *                date: 2020-01-01
 *                duree: 60
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 * 
 *    post:
 *      summary: Create an consultation
 *      description: consultation is created with one praticien and one client.
 *      tags: [Consultations]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - idpraticien
 *                - idclient
 *                - date
 *                - duree
 *              properties:
 *                idpraticien:
 *                  type: integer
 *                idclient:
 *                  type: integer 
 *                date:
 *                  type: string
 *                duree:
 *                  type: integer 
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  idclient:
 *                    type: integer
 *                  date:
 *                    type: string
 *                  duree:
 *                    type: integer
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * path:
 *  /consultations/{id}:
 *    get:
 *      summary: Get a consultation
 *      description: To check the id before deleting.
 *      tags: [Consultations]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Consultation id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  idclient:
 *                    type: integer
 *                  date:
 *                    type: string
 *                  duree:
 *                    type: integer
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                idclient: 2
 *                date: 2020-01-01
 *                duree: 60
 *    delete:
 *      summary: Delete a Consultation
 *      description: Delete by id
 *      tags: [Consultations]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Consultation id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *    patch:
 *      summary: Update an Consultation
 *      description: To update an Consultation.
 *      tags: [Consultations]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Consultation id
 *      requestBody:
 *        required : true
 *        content:
 *          application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  idpraticien:
 *                    type: integer
 *                  idclient:
 *                    type: integer
 *                  date:
 *                    type: string
 *                  duree:
 *                    type: integer
 *              example:
 *                idpraticien: 1
 *                idclient: 2
 *                date: 2020-01-01
 *                duree: 60
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  idclient:
 *                    type: integer
 *                  date:
 *                    type: string
 *                  duree:
 *                    type: integer
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                idclient: 2
 *                date: 2020-01-01
 *                duree: 60
 */


 
 /**
 * @swagger
 * path:
 *  /consultations/client/{idclient}:
 *    get:
 *      summary: Get a consultation by idclient
 *      description: To check consultation's client.
 *      tags: [Consultations]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: idclient
 *          required: true
 *          schema:
 *            type: string
 *          description: ID client
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  idclient:
 *                    type: integer
 *                  date:
 *                    type: string
 *                  duree:
 *                    type: integer
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                idclient: 2
 *                date: 2020-01-01
 *                duree: 60
 */

 /**
 * @swagger
 * path:
 *  /consultations/praticien/{idpraticien}:
 *    get:
 *      summary: Get a consultation by idpraticien
 *      description: To check consultation's praticien.
 *      tags: [Consultations]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: idpraticien
 *          required: true
 *          schema:
 *            type: string
 *          description: ID praticien
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  idpraticien:
 *                    type: integer
 *                  idclient:
 *                    type: integer
 *                  date:
 *                    type: string
 *                  duree:
 *                    type: integer
 *              example:
 *                id: 999
 *                idpraticien: 1
 *                idclient: 2
 *                date: 2020-01-01
 *                duree: 60
 */
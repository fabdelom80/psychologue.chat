const express = require('express');
const auth = require('../../middlewares/auth');

const router = express.Router();
const { abonnementController } = require('../../controllers');

router
  .route('/')
  .post(auth('pra'), abonnementController.createAbonnement)
  .get();

router
  .route('/abomensuel/:id')
  .get(auth('pra'), abonnementController.createTempAbo1Month)

  
router
  .route('/aboannuel/:id')
  .get(auth('pra'),  abonnementController.createTempAbo1Year)

router
  .route('/aboavie/:id')
  .get(auth('pra'),abonnementController.createTempAboFullLife)


module.exports = router;



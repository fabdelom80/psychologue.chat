const express = require('express');
const authRoute = require('./auth.route');
const docsRoute = require('./docs.route');
const consultationRoute = require('./consultation.route');
const clientsRoute = require('./clients.route');
const praticienRoute = require('./praticien.route');
const abonnementRoute = require('./abonnement.route');
const demandePaiementRoute = require('./demandePaiement.route');
const webhookRoute = require('./webhook.route');
const config = require('../../config/config');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/clients',
    route: clientsRoute,
  },
  {
    path: '/praticiens',
    route: praticienRoute,
  },
  {
    path: '/consultations',
    route: consultationRoute,
  },
  {
    path: '/abonnements',
    route: abonnementRoute,
  },
  {
    path: '/demandepaiement',
    route: demandePaiementRoute,
  },
  {
    path: '/webhook',
    route: webhookRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;

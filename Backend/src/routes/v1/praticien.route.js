const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const praticienValidation = require('../../validations/praticien.validation');
const praticienController = require('../../controllers/praticien.controller');
const authController = require('../../controllers/auth.controller');

const router = express.Router();

router
  .route('/')
  .get(auth('admin'), validate(praticienValidation.getPraticiens), praticienController.getPraticiens)
  .post(validate(praticienValidation.createPraticien), authController.register);

router
  .route('/valid/:value')
  .get(auth('pra'), validate(praticienValidation.getPraticiensByValidite), praticienController.getPraticiensByValidite);

router
  .route('/email/:email')
  .get(praticienController.getPraticienByEmail);

router
  .route('/emailboolean/:email')
  .get(praticienController.getBooleanPraticienByEmail);

router
  .route('/adeli/:adeli')
  .get(praticienController.getPraticienByAdeli);

router
  .route('/siret/:siret')
  .get(praticienController.getPraticienBySiret);

  router
  .route('/available/')
  .get(auth('pra'), validate(praticienValidation.getPraticiensAvailable), praticienController.getPraticiensAvailable);

router
  .route('/:id')
  .get(auth('pra'), validate(praticienValidation.getPraticien), praticienController.getPraticien)
  .patch(auth('admin'), validate(praticienValidation.updatePraticien), praticienController.updatePraticien)
  .delete(auth('admin'), validate(praticienValidation.deletePraticien), praticienController.deletePatricien);


/**
 * @swagger
 * tags:
 *   name: Praticien
 *   description: Praticien management and retrieval
 */

/**
 * @swagger
 * path:
 *  /praticiens:
 *    get:
 *      summary: Get all praticiens from database
 *      description: Only admins can retrieve all praticien.
 *      tags: [Praticien]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *                  mdp:
 *                    type: string
 *                  tel:
 *                    type: string
 *                  adresse:
 *                    type: string
 *                  siret:
 *                    type: integer
 *                  adeli:
 *                    type: integer
 *                  valid:
 *                    type: integer
 *                  online:
 *                    type: boolean
 *              example:
 *                id: 999
 *                email: fake@example.com
 *                nom: fakeNom
 *                prenom: fakePrenom
 *                mdp: hashed password
 *                tel: '0102030405'
 *                adresse: 1 rue de la ligue 2 69001 LYON
 *                siret: 00000000000000
 *                adeli: 0000000000
 *                valid: 1
 *                online: false
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *
 *    post:
 *      summary: Create a praticien with valid value "1" (Attente verification)
 *      description: Praticien is created to participate in a consultation.
 *      tags: [Praticien]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - email
 *                - nom
 *                - prenom
 *                - mdp
 *                - tel
 *                - siret
 *                - adeli
 *              properties:
 *                email:
 *                  type: string
 *                  format: email
 *                nom:
 *                  type: string 
 *                prenom:
 *                  type: string
 *                mdp:
 *                  type: string
 *                tel:
 *                  type: string
 *                adresse:
 *                  type: string
 *                siret:
 *                  type: integer
 *                adeli:
 *                  type: integer
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *                  mdp:
 *                    type: string
 *                  tel:
 *                    type: string
 *                  adresse:
 *                    type: string
 *                  siret:
 *                    type: integer
 *                  adeli:
 *                    type: integer
 *                  valid:
 *                    type: integer
 *                  online:
 *                    type: boolean
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden' 
 */

/**
 * @swagger
 * path:
 *  /praticiens/{id}:
 *    get:
 *      summary: Get a praticien by ID
 *      description: To check the id before deleting.
 *      tags: [Praticien]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Praticien id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *                  mdp:
 *                    type: string
 *                  tel:
 *                    type: string
 *                  adresse:
 *                    type: string
 *                  siret:
 *                    type: integer
 *                  adeli:
 *                    type: integer
 *                  valid:
 *                    type: integer
 *                  online:
 *                    type: boolean
 *              example:
 *                id: 999
 *                email: fake@example.com
 *                nom: fakeNom
 *                prenom: fakePrenom
 *                mdp: hashed password
 *                tel: '0102030405'
 *                adresse: 1 rue de la ligue 2 69001 LYON
 *                siret: 00000000000000
 *                adeli: 0000000000
 *                valid: 1
 *                online: false
 *    delete:
 *      summary: Delete a praticien
 *      description: Delete by id
 *      tags: [Praticien]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: integer
 *          description: Praticien id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 * 
 *    patch:
 *      summary: Update a praticien
 *      description: Only admins can update praticien.
 *      tags: [Praticien]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Pratician id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                email:
 *                  type: string
 *                  format: email
 *                  description: must be unique
 *                password:
 *                  type: string
 *                  format: password
 *                  minLength: 8
 *                  description: At least one number and one letter
 *              example:
 *                id: 999
 *                email: fake@example.com
 *                nom: fakeNom
 *                prenom: fakePrenom
 *                mdp: hashed password
 *                tel: '0102030405'
 *                adresse: 1 rue de la ligue 2 69001 LYON
 *                siret: 00000000000000
 *                adeli: 0000000000
 *                valid: 1
 *                online: false
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/User'
 *        "400":
 *          $ref: '#/components/responses/DuplicateEmail'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 * path:
 *  /praticiens/valid/{valid}:
 *    get:
 *      summary: Get all praticiens from database according to "valid" value
 *      description: Only admins can retrieve all praticien for check SIRET, ADELI... .
 *      tags: [Praticien]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: valid
 *          required: true
 *          schema:
 *            type: integer
 *          description: Praticien "valid" value
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *                  mdp:
 *                    type: string
 *                  tel:
 *                    type: string
 *                  adresse:
 *                    type: string
 *                  siret:
 *                    type: integer
 *                  adeli:
 *                    type: integer
 *                  valid:
 *                    type: integer
 *                  online:
 *                    type: boolean
 *              example:
 *                id: 999
 *                email: fake@example.com
 *                nom: fakeNom
 *                prenom: fakePrenom
 *                mdp: hashed password
 *                tel: '0102030405'
 *                adresse: 1 rue de la ligue 2 69001 LYON
 *                siret: 00000000000000
 *                adeli: 0000000000
 *                valid: 1
 *                online: false
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * path:
 *  /praticiens/available:
 *    get:
 *      summary: Get all praticiens available and ready for consultation
 *      description: Praticien disponible en attente de consultation
 *      tags: [Praticien]
 *      security:
 *        - bearerAuth: []
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  id:
 *                    type: integer
 *                    description : sequence 
 *                  email:
 *                    type: string
 *                    format: email
 *                  nom:
 *                    type: string
 *                  prenom:
 *                    type: string
 *                  mdp:
 *                    type: string
 *                  tel:
 *                    type: string
 *                  adresse:
 *                    type: string
 *                  siret:
 *                    type: integer
 *                  adeli:
 *                    type: integer
 *                  valid:
 *                    type: integer
 *                  online:
 *                    type: boolean
 *              example:
 *                id: 999
 *                email: fake@example.com
 *                nom: fakeNom
 *                prenom: fakePrenom
 *                mdp: hashed password
 *                tel: '0102030405'
 *                adresse: 1 rue de la ligue 2 69001 LYON
 *                siret: 00000000000000
 *                adeli: 0000000000
 *                valid: 1
 *                online: false
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */

module.exports = router;
const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');

const router = express.Router();
const { webhookController } = require('../../controllers');
const bodyParser = require('body-parser');




router
  .route('/')
  .post(bodyParser.raw({type: 'application/json'}), webhookController.createWebhook)

module.exports = router;



const httpStatus = require('http-status');
const { abonnements } = require('../../models');
const ApiError = require('../utils/ApiError');
const stripe = require('stripe')(process.env.StripekeySecret);

/**
 * Create a abonnement
 * @param {Object} abonnementBody
 * @returns {Promise<abonnements>}
 */
const createAbonnement = async (idPraticien, month) => {
  

  let dateDebutAbo = await getDateEndAboByPraticien(idPraticien);
  let dateFinAbo = new Date(dateDebutAbo);
  dateFinAbo = dateFinAbo.setMonth(dateFinAbo.getMonth()+month);
  const body = {
    "idpraticien": idPraticien,
    "datedebut": dateDebutAbo,
    "datefin": dateFinAbo
  };
  const abonnement = await abonnements.create(body);
  return abonnement; 
};


const getAbonnements = async () => {
  const abonnement = await abonnements.findAll();
  return abonnement;
};


/**
 * Get abonnement by id
 * @param {ObjectId} id
 * @returns {Promise<abonnements>}
 */
const getAbonnementById = async (id) => {
  return abonnements.findByPk(id);
};



/**
 * Update abonnement by id
 * @param {String} abonnementId
 * @param {Object} updateBody
 * @returns {Promise<abonnements>}
 */


const updateAbonnementById = async (abonnementId, updateBody) => {
  const abonnement = await getAbonnementById(abonnementId);
  if (!abonnement) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Abonnement not found');
  }
  Object.assign(abonnement, updateBody);
  await abonnement.save();
  return abonnement;
};

/**
 * Delete abonnement by id
 * @param {String} abonnementId
 * @returns {Promise<abonnements>}
 */
const deleteAbonnementById = async (abonnementId) => {
  const abonnement = await getAbonnementById(abonnementId);
  if (!abonnement) {
    throw new ApiError(httpStatus.NOT_FOUND, 'abonnement not found');
  }
  await abonnement.destroy();
  return console.log(`L'abonnement dont l'id est :  ${abonnementId} a été supprimé` );
};



/**
 * Get abonnement by praticien
 * @param {string} idpraticien
 * @returns {Promise<abonnements>}
 */
const getAbonnementByPraticien = async (idpraticien) => {
  return abonnements.findAll({ where : {idpraticien : idpraticien}});
};


/**
 * Get last abonnement by praticien
 * @param {string} idpraticien
 * @returns {Promise<abonnements>}
 */
const getLastAbonnementByPraticien = async (idpraticien) => {
  return abonnements.findAll({ 
    limit: 1,
    where : {idpraticien : idpraticien},
        order: [
          ['datefin', 'DESC'],
      ],
    });
};

const getDateEndAboByPraticien = async (idpraticien) => {
  
  dataFinAbo = await getLastAbonnementByPraticien(idpraticien);
  dateJour =  new Date(Date.now());
    if(dataFinAbo.length > 0) {
    finAbo = await dataFinAbo[0].dataValues.datefin;
    finAbo =  new Date(finAbo);
    if(finAbo >= dateJour) {
      return finAbo;
    }
  }
  return dateJour; 
}

const createTempAbo = async (email, product, amount) => {
  const session = await stripe.checkout.sessions.create({
    payment_method_types: ['card'],
    customer_email: email,
  line_items: [{
  price_data: {
    product: product,
    unit_amount: amount,
    currency: 'eur',
  },
  quantity: 1,
}],
mode: 'payment',
success_url: process.env.URL_FRONT + "profil",
cancel_url: process.env.URL_FRONT + "echec-paiement",
});
return session;
}


module.exports = {
  createAbonnement,
  getAbonnements,
  getAbonnementById,
  updateAbonnementById,
  deleteAbonnementById,
  getAbonnementByPraticien,
  getLastAbonnementByPraticien,
  getDateEndAboByPraticien,
  createTempAbo
};

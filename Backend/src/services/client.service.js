const httpStatus = require('http-status');
const { cli } = require('winston/lib/winston/config');
const { clients } = require('../../models');
const ApiError = require('../utils/ApiError');

const queryClients = async () => {
  const result = await clients.findAll();
  return result;
};

const queryClient = async (id) => {
  //const result = await clients.findAll( {where : { id : id }}); -> renvoi un tableau 
  const result = await clients.findByPk(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  return result;
};

const createClient = async (clientBody) => {
  const result = await clients.create(clientBody);
  return result;
};

const deleteClient = async (id) => {
  const client = await queryClient(id);
  if (!client) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await client.destroy();
  return client;
};

module.exports = {
  queryClients,
  queryClient,
  createClient,
  deleteClient
};

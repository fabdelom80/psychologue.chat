const httpStatus = require('http-status');
const { consultations } = require('../../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a consultation
 * @param {Object} consultationBody
 * @returns {Promise<consultations>}
 */
const createConsultation = async (consultationBody) => {
  const consultation = await consultations.create(consultationBody);
  return consultation;
};


const getConsultations = async () => {
  const result = await consultations.findAll();
  return result;
};


/**
 * Get consultation by id
 * @param {ObjectId} id
 * @returns {Promise<consultations>}
 */
const getConsultationById = async (id) => {
  return consultations.findByPk(id);
};



/**
 * Update consultation by id
 * @param {String} consultationId
 * @param {Object} updateBody
 * @returns {Promise<consultations>}
 */


const updateConsultationById = async (consultationId, updateBody) => {
  const consultation = await getConsultationById(consultationId);
  if (!consultation) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Consultation not found');
  }
  Object.assign(consultation, updateBody);
  await consultation.save();
  return consultation;
};

/**
 * Delete consultation by id
 * @param {String} consultationId
 * @returns {Promise<consultations>}
 */
const deleteConsultationById = async (consultationId) => {
  const consultation = await getConsultationById(consultationId);
  if (!consultation) {
    throw new ApiError(httpStatus.NOT_FOUND, 'consultation not found');
  }
  await consultation.destroy();
  return console.log(`La consultation dont l'id est :  ${consultationId} a été supprimée` );
};


/**
 * Get consultation by client
 * @param {string} idclient
 * @returns {Promise<consultations>}
 */
const getConsultationByClient = async (idclient) => {
  return consultations.findAll({ where : {idclient : idclient}});
};

/**
 * Get consultation by praticien
 * @param {string} idpraticien
 * @returns {Promise<consultations>}
 */
const getConsultationByPraticien = async (idpraticien) => {
  return consultations.findAll({ where : {idpraticien : idpraticien}});
};



module.exports = {
  createConsultation,
  getConsultations,
  getConsultationById,
  updateConsultationById,
  deleteConsultationById,
  getConsultationByClient,
  getConsultationByPraticien,
};

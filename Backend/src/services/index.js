module.exports.authService = require('./auth.service');
module.exports.emailService = require('./email.service');
module.exports.tokenService = require('./token.service');
module.exports.clientService = require('./client.service');
module.exports.praticienService = require('./praticien.service');
module.exports.consultationService = require('./consultation.service');
module.exports.abonnementService = require('./abonnement.service');
module.exports.stripeaboService = require('./stripeabo.service');
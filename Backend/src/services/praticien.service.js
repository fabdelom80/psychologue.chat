const httpStatus = require('http-status');
const { cli } = require('winston/lib/winston/config');
const { praticiens } = require('../../models');
const ApiError = require('../utils/ApiError');
const { roles } = require('../config/roles')
const { Op } = require("sequelize");
const bcrypt = require('bcryptjs');

const getPraticiens = async () => {
  const result = await praticiens.findAll({where: {
    role: { [Op.in]: [roles[0], roles[2]]}
  }});
  return result;
};

const getPraticien = async (id) => {
  const result = await praticiens.findByPk(id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  return result;
};

const getPraticienByEmail = async (email) => {
  const result = await praticiens.findOne({where: {
    email:email
  }});
  // if (!result) {
  //   throw new ApiError(httpStatus.NOT_FOUND, 'Praticien not found');
  // }
  return result;
}

const getBooleanPraticienByEmail = async (email) => {
  const result = await praticiens.findOne({where: {
    email:email
  }});                                            
  if (!result) {
    return false;
  }
  return true;
}

const getPraticienByAdeli = async (adeli) => {
  const result = await praticiens.findOne({where: {
    adeli:adeli
  }});                                            
  if (!result) {
    return false;
  }
  return true;
}

const getPraticienBySiret = async (siret) => {
  const result = await praticiens.findOne({where: {
    siret:siret
  }});                                         
  if (!result) {
    return false;
  }
  return true;
}

const getPraticiensByValidite = async (validValue) => {
  const result = await praticiens.findAll({ where : { 
    valid : validValue, 
    role: { [Op.in]: [roles[0], roles[2]]}
  }});
  return result;
};

const getPraticiensAvailable = async () => {
  const result = await praticiens.findAll({ where : { 
    valid : 4, online : true,
    role: { [Op.in]: [roles[0], roles[2]]}
  }});
  return result;
};

const createPraticien = async (praticienBody) => {
  const praticien = praticienBody;
  praticien.mdp = await bcrypt.hash(praticien.mdp, 8);
  const result = await praticiens.create(praticien);
  return result;
};

const updatePraticienById = async (praticienId, updateBody) => {
  if(updateBody.mdp != null) {
    updateBody.mdp = await bcrypt.hash(updateBody.mdp, 8);
  }
  const praticien = await getPraticien(praticienId);
  if (!praticien) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  Object.assign(praticien, updateBody);
  await praticien.save();
  return praticien;
};

const deletePraticien = async (id) => {
  const praticien = await getPraticien(id);
  if (!praticien) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await praticien.destroy();
  return praticien;
};

module.exports = {
    getPraticiens,
    getPraticien,
    updatePraticienById,
    getPraticiensByValidite,
    getPraticiensAvailable,
    createPraticien,
    deletePraticien,
    getPraticienByEmail,
    getPraticienBySiret,
    getPraticienByAdeli,
    getBooleanPraticienByEmail
};
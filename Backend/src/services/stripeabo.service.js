const httpStatus = require('http-status');
const { stripeabo } = require('../../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a abonnement
 * @param {Object} abonnementBody
 * @returns {Promise<abonnements>}
 */
const createStripeabo = async (Body) => {
  const result = await stripeabo.create(Body);
  return result;
};


/**
 * Get abonnement by id
 * @param {ObjectId} id
 * @returns {Promise<abonnements>}
 */
const getStripeaboById = async (id) => {
  const result = await stripeabo.findOne({where: {
    idstripe:id
  }});                                            
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'stripeabo not found');
  }
  return result;
};

const getMonthAbo = (price) => {
  if(session.amount_total == 1000) {
    return 1;
  }
  else if(session.amount_total == 10000){
    return 12;
  }
  else if(session.amount_total == 55000){
    return 1200;
  }
  return 0;
};




/**
 * Update abonnement by id
 * @param {String} abonnementId
 * @param {Object} updateBody
 * @returns {Promise<abonnements>}
 */


const updateStripeaboById = async (Id, updateBody) => {
  const result = await getStripeaboById(Id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'stripeabo not found');
  }
  Object.assign(result, updateBody);
  await result.save();
  return result;
};




module.exports = {
  createStripeabo,
  getStripeaboById,
  updateStripeaboById,
  getMonthAbo
};

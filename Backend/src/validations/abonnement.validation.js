const Joi = require('joi');

const createAbonnement = {
  body: Joi.object().keys({
    id: Joi.number().integer(),
    idpraticien: Joi.number().integer().required(),
    datedebut: Joi.string().required(),
    datefin: Joi.string().required(),
  }),
};

const getAbonnements = {
  query: Joi.object().keys({
    idpraticien: Joi.number().integer(),
    datedebut: Joi.string(),
    datefin: Joi.string(),
  }),
};

const getAbonnement = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};


const updateAbonnement = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
  body: Joi.object()
    .keys({
      idpraticien: Joi.number().integer(),
      datedebut: Joi.string(),
      datefin: Joi.string(),
    })
    .min(1),
};

const deleteAbonnement = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};


const getAbonnementByPraticien = {
  params: Joi.object().keys({
    idpraticien: Joi.string().required(),
  }),
};

const getLastAbonnementByPraticien = {
  params: Joi.object().keys({
    idpraticien: Joi.string().required(),
  }),
};

module.exports = {
  createAbonnement,
  getAbonnements,
  getAbonnement,
  updateAbonnement,
  deleteAbonnement,
  getAbonnementByPraticien,
  getLastAbonnementByPraticien,
};

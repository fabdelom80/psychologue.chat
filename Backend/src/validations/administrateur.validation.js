const Joi = require('joi');
const { password, objectId } = require('./custom.validation');

const createAdministrateur = {
  body: Joi.object().keys({
    id: Joi.number().integer(),
    email: Joi.string().required().email(),
    mdp: Joi.string().required(),
  }),
};

const getAdministrateurs = {
  query: Joi.object().keys({
    email: Joi.string().email(),
  }),
};

const getAdministrateur = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
};

const getAdministrateurByEmail = {
  params: Joi.object().keys({
    email: Joi.string(),
  }),
};

const updateAdministrateur = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
  body: Joi.object()
    .keys({
      email: Joi.string().email(),
      mdp: Joi.string(),
    })
    .min(1),
};

const deleteAdministrateur = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
};

module.exports = {
  createAdministrateur,
  getAdministrateurs,
  getAdministrateur,
  updateAdministrateur,
  deleteAdministrateur,
  getAdministrateurByEmail,
};

const Joi = require('joi');

const createClient = {
    body: Joi.object().keys({
        ///id : Joi.number().integer().required(),
        email: Joi.string().required().email(),
        nom: Joi.string().required(),
        prenom: Joi.string().required(),
    }),
  };

  const getClients = {
    query: Joi.object().keys({
      email: Joi.string(),
      nom: Joi.string(),
      prenom: Joi.string(),
      sortBy: Joi.string(),
      limit: Joi.number().integer(),
      page: Joi.number().integer(),
    }),
  };

  const getClient = {
    params: Joi.object().keys({
      id: Joi.string(),
    }),
  };

  const deleteClient = {
    params: Joi.object().keys({
      id: Joi.string(),
    }),
  };

module.exports = {
    createClient,
    getClients,
    getClient,
    deleteClient
  };
  
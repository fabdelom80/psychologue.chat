const Joi = require('joi');

const createConsultation = {
  body: Joi.object().keys({
    id: Joi.number().integer(),
    idpraticien: Joi.number().integer().required(),
    idclient: Joi.number().integer().required(),
    date: Joi.string().required(),
    duree: Joi.number().integer().required(),
  }),
};

const getConsultations = {
  query: Joi.object().keys({
    idpraticien: Joi.number().integer(),
    idclient: Joi.number().integer(),
    date: Joi.string(),
    duree: Joi.number().integer(),
  }),
};

const getConsultation = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};


const updateConsultation = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
  body: Joi.object()
    .keys({
      idpraticien: Joi.number().integer(),
      idclient: Joi.number().integer(),
      date: Joi.string(),
      duree: Joi.number().integer(),
    })
    .min(1),
};

const deleteConsultation = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

const getConsultationByClient = {
  params: Joi.object().keys({
    idclient: Joi.string().required(),
  }),
};

const getConsultationByPraticien = {
  params: Joi.object().keys({
    idpraticien: Joi.string().required(),
  }),
};

module.exports = {
  createConsultation,
  getConsultations,
  getConsultation,
  updateConsultation,
  deleteConsultation,
  getConsultationByClient,
  getConsultationByPraticien,
};

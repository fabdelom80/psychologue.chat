const Joi = require('joi');

const createPraticien = {
  body: Joi.object().keys({
      id : Joi.number().integer(),
      email: Joi.string().required().email(),
      nom: Joi.string().required(),
      prenom: Joi.string().required(),
      mdp: Joi.string().required(),
      tel: Joi.string().required(),
      adresse: Joi.string(),
      siret: Joi.string().required(),
      adeli: Joi.string().required()
  }),
};

const getPraticiens = {
    /*query: Joi.object().keys({
      email: Joi.string(),
      nom: Joi.string(),
      prenom: Joi.string(),
    }),*/
};

const getPraticien = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
};

const getPraticiensByValidite = {
  query: Joi.object().keys({
    email: Joi.string(),
    nom: Joi.string(),
    prenom: Joi.string(),
  }),
};

const getPraticiensAvailable = {
  /*query: Joi.object().keys({
    valid: Joi.required(),
    online: Joi.required()
  }),*/
};

const updatePraticien = {
  params: Joi.object().keys({
    id: Joi.required(),
  }),
  body: Joi.object()
    .keys({
      email: Joi.string().email(),
      nom: Joi.string(),
      prenom: Joi.string(),
      mdp: Joi.string(),
      tel: Joi.string(),
      adresse: Joi.string(),
      siret: Joi.string(),
      adeli: Joi.string(),
      valid : Joi.number().integer(),
      online: Joi.boolean()
    })
    .min(1),
};

const deletePraticien = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
};

module.exports = {
  getPraticiens,
  getPraticien,
  getPraticiensByValidite,
  getPraticiensAvailable,
  createPraticien,
  updatePraticien,
  deletePraticien
};
  
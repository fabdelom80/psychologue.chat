const bcrypt = require('bcryptjs');
const faker = require('faker');
const { praticiens } = require('../../models');

const password = 'password1';
const salt = bcrypt.genSaltSync(8);
const hashedPassword = bcrypt.hashSync(password, salt);

const userOne = {
  id: faker.random.number(9999),
  nom: faker.name.findName(),
  prenom: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  tel: faker.random.number(99999),
  mdp: 'password1',
  siret: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  }),
  adeli: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  })
};

const userTwo = {
  id: faker.random.number(9999),
  nom: faker.name.findName(),
  prenom: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  tel: faker.random.number(99999),
  mdp: 'password1',
  siret: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  }),
  adeli: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  })
};

const praAdmin = {
  id: faker.random.number(99999),
  nom: faker.name.findName(),
  prenom: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  tel: faker.random.number(99999),
  mdp: 'password1',
  siret: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  }),
  adeli: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  }),
  role: 'pra-admin'
};

const admin = {
  id: faker.random.number(99999),
  nom: faker.name.findName(),
  prenom: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  tel: faker.random.number(99999),
  mdp: 'password1',
  siret: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  }),
  adeli: faker.random.number({
    'min': 1000000000,
    'max': 9999999999
  }),
  role: 'admin'
};

const insertUsers = async (users) => {
  // users.forEach(async (user) => {
  await praticiens.bulkCreate(users.map((user) => ({ ...user, mdp: hashedPassword })))
    // })

};

module.exports = {
  userOne,
  userTwo,
  admin,
  praAdmin,
  insertUsers,
};

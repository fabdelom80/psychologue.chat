const request = require('supertest');
const faker = require('faker');
const httpStatus = require('http-status');
const app = require('../../src/app');
const setupTestDB = require('../utils/setupTestDB');
const { praticiens } = require('../../models');
const { userOne, userTwo, admin, praAdmin, insertUsers } = require('../fixtures/user.fixture');
const { userOneAccessToken, adminAccessToken, praAdminAccessToken } = require('../fixtures/token.fixture');

setupTestDB();

describe('Praticien routes', () => {
  describe('POST /v1/praticiens', () => {
    let newUser;

    beforeEach(() => {
      newUser = {
        id: faker.random.number(99999),
        nom: faker.name.findName(),
        prenom: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        tel: faker.random.number(99999).toString(),
        mdp: 'password1',
        siret: faker.random.number({
          'min': 1000000000,
          'max': 9999999999
        }).toString(),
        adeli: faker.random.number({
          'min': 1000000000,
          'max': 9999999999
        }).toString()
      };
    });


    test('should return 201 and successfully create new praticien if data is ok', async () => {
      // await insertUsers([userOne]);

      const res = await request(app)
        .post('/v1/praticiens')
        // .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.CREATED);

      expect(res.body).not.toHaveProperty('mdp');
      expect(res.body.user).toEqual(
        {
          id: expect.anything(),
          nom: newUser.nom,
          prenom: newUser.prenom,
          email: newUser.email,
          role: "pra",
          siret: newUser.siret.toString(),
          adeli: newUser.adeli.toString(),
          tel: newUser.tel.toString(),
          valid: 1,
          online: false,
          adresse: null
        }
      );

      const dbUser = await praticiens.findByPk(res.body.user.id);
      expect(dbUser).toBeDefined();
      expect(dbUser.mdp).not.toBe(newUser.mdp);
      // expect(dbUser).toMatchObject({ name: newUser.name, email: newUser.email, role: newUser.role });
    });



    // test('should return 401 error if access token is missing', async () => {
    //   await request(app).post('/v1/praticiens').send(newUser).expect(httpStatus.UNAUTHORIZED);
    // });

    // test('should return 403 error if logged in user is not admin', async () => {
    //   await insertUsers([userOne]);

    //   await request(app)
    //     .post('/v1/praticiens')
    //     .set('Authorization', `Bearer ${userOneAccessToken}`)
    //     .send(newUser)
    //     .expect(httpStatus.FORBIDDEN);
    // });

    test('should return 400 error if email is invalid', async () => {
      await insertUsers([admin]);
      newUser.email = 'invalidEmail';

      await request(app)
        .post('/v1/praticiens')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(newUser)
        .expect(httpStatus.BAD_REQUEST);
    });

    // test('should return 400 error if email is already used', async () => {
    //   await insertUsers([admin, userOne]);
    //   newUser.email = userOne.email;

    //   await request(app)
    //     .post('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .send(newUser)
    //     .expect(httpStatus.BAD_REQUEST);
    // });

    // test('should return 400 error if password length is less than 8 characters', async () => {
    //   await insertUsers([admin]);
    //   newUser.password = 'passwo1';

    //   await request(app)
    //     .post('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .send(newUser)
    //     .expect(httpStatus.BAD_REQUEST);
    // });

    // test('should return 400 error if password does not contain both letters and numbers', async () => {
    //   await insertUsers([admin]);
    //   newUser.password = 'password';

    //   await request(app)
    //     .post('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .send(newUser)
    //     .expect(httpStatus.BAD_REQUEST);

    //   newUser.password = '1111111';

    //   await request(app)
    //     .post('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .send(newUser)
    //     .expect(httpStatus.BAD_REQUEST);
    // });

    // test('should return 400 error if role is neither user nor admin', async () => {
    //   await insertUsers([admin]);
    //   newUser.role = 'invalid';

    //   await request(app)
    //     .post('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .send(newUser)
    //     .expect(httpStatus.BAD_REQUEST);
    // });
  });

  describe('GET /v1/praticiens', () => {
    // test('should return 200 and apply the default query options', async () => {
    //   await insertUsers([userOne, userTwo, admin]);

    //   const res = await request(app)
    //     .get('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .send()
    //     .expect(httpStatus.OK);

    //   expect(res.body).toEqual({
    //     results: expect.any(Array),
    //     page: 1,
    //     limit: 10,
    //     totalPages: 1,
    //     totalResults: 3,
    //   });
    //   expect(res.body.results).toHaveLength(3);
    //   expect(res.body.results[0]).toEqual({
    //     id: userOne._id.toHexString(),
    //     name: userOne.name,
    //     email: userOne.email,
    //     role: userOne.role,
    //   });
    // });

    test('should return 401 if access token is missing', async () => {
      await insertUsers([userOne, userTwo, admin]);

      await request(app).get('/v1/praticiens').send().expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 403 if a non-admin is trying to access all users', async () => {
      await insertUsers([userOne, userTwo, admin]);

      await request(app)
        .get('/v1/praticiens')
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    // test('should correctly apply filter on name field', async () => {
    //   await insertUsers([userOne, userTwo, admin]);

    //   const res = await request(app)
    //     .get('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .query({ name: userOne.name })
    //     .send()
    //     .expect(httpStatus.OK);

    //   expect(res.body).toEqual({
    //     results: expect.any(Array),
    //     page: 1,
    //     limit: 10,
    //     totalPages: 1,
    //     totalResults: 1,
    //   });
    //   expect(res.body.results).toHaveLength(1);
    //   expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    // });

    // test('should correctly apply filter on role field', async () => {
    //   await insertUsers([userOne, userTwo, admin]);

    //   const res = await request(app)
    //     .get('/v1/praticiens')
    //     .set('Authorization', `Bearer ${adminAccessToken}`)
    //     .query({ role: 'user' })
    //     .send()
    //     .expect(httpStatus.OK);

    //   expect(res.body).toEqual({
    //     results: expect.any(Array),
    //     page: 1,
    //     limit: 10,
    //     totalPages: 1,
    //     totalResults: 2,
    //   });
    //   expect(res.body.results).toHaveLength(2);
    //   expect(res.body.results[0].id).toBe(userOne._id.toHexString());
    //   expect(res.body.results[1].id).toBe(userTwo._id.toHexString());
    // });


    test('should only return the praticiens with pra role', async () => {
      await insertUsers([userOne, userTwo, admin, praAdmin]);

      const res = await request(app)
        .get('/v1/praticiens')
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toHaveLength(3);
      expect(res.body[0].id).toBe(userOne.id);
      expect(res.body[1].id).toBe(userTwo.id);
      expect(res.body[2].id).toBe(praAdmin.id);
    });
  });

  describe('GET /v1/praticiens/:id', () => {
    test('should return 200 and the user object if data is ok', async () => {
      await insertUsers([userOne]);

      const res = await request(app)
        .get(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty('mdp');
      expect(res.body).toEqual({
        id: userOne.id,
        nom: userOne.nom,
        prenom: userOne.prenom,
        email: userOne.email,
        role: "pra",
        siret: userOne.siret.toString(),
        adeli: userOne.adeli.toString(),
        tel: userOne.tel.toString(),
        valid: 1,
        online: false,
        adresse: null
      });
    });

    test('should return 401 error if access token is missing', async () => {
      await insertUsers([userOne]);

      await request(app).get(`/v1/praticiens/${userOne.id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 403 error if non pra is trying to get a praticien', async () => {
      await insertUsers([userOne, admin]);

      await request(app)
        .get(`/v1/praticiens/${userTwo.id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test('should return 200 and the praticien object if praticien is trying to get a praticien', async () => {
      await insertUsers([userOne, praAdmin]);

      await request(app)
        .get(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${praAdminAccessToken}`)
        .send()
        .expect(httpStatus.OK);
    });



    test('should return 404 error if user is not found', async () => {
      await insertUsers([praAdmin]);

      await request(app)
        .get(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${praAdminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe('DELETE /v1/praticiens/:id', () => {
    test('should return 204 if pra-admin deletes a praticien', async () => {
      await insertUsers([userOne, praAdmin]);

      await request(app)
        .delete(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${praAdminAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbUser = await praticiens.findByPk(userOne.id);
      expect(dbUser).toBeNull();
    });

    test('should return 401 error if access token is missing', async () => {
      await insertUsers([userOne]);

      await request(app).delete(`/v1/praticiens/${userOne.id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 404 error if praticien is not found', async () => {
      await insertUsers([admin]);

      await request(app)
        .delete(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe('PATCH /v1/praticiens/:id', () => {
    test('should return 200 and successfully update praticien if data is ok', async () => {
      await insertUsers([userOne]);
      const updateBody = {
        nom: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        mdp: 'newPassword1',
      };

      const res = await request(app)
        .patch(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);

      expect(res.body).not.toHaveProperty('mdp');
      expect(res.body).toEqual({
        id: expect.anything(),
        nom: updateBody.nom,
        prenom: userOne.prenom,
        email: updateBody.email,
        role: "pra",
        siret: userOne.siret.toString(),
        adeli: userOne.adeli.toString(),
        tel: userOne.tel.toString(),
        valid: 1,
        online: false,
        adresse: null
      });

      const dbUser = await praticiens.findByPk(userOne.id);
      expect(dbUser).toBeDefined();
      expect(dbUser.password).not.toBe(updateBody.mdp);
    });

    test('should return 401 error if access token is missing', async () => {
      await insertUsers([userOne]);
      const updateBody = { nom: faker.name.findName() };

      await request(app).patch(`/v1/praticiens/${userOne.id}`).send(updateBody).expect(httpStatus.UNAUTHORIZED);
    });

    test('should return 403 if user is updating another user', async () => {
      await insertUsers([userOne, userTwo]);
      const updateBody = { nom: faker.name.findName() };

      await request(app)
        .patch(`/v1/praticiens/${userTwo.id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.FORBIDDEN);
    });

    test('should return 200 and successfully update user if admin is updating another user', async () => {
      await insertUsers([userOne, admin]);
      const updateBody = { nom: faker.name.findName() };

      await request(app)
        .patch(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.OK);
    });

    test('should return 404 if admin is updating another user that is not found', async () => {
      await insertUsers([admin]);
      const updateBody = { nom: faker.name.findName() };

      await request(app)
        .patch(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${adminAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.NOT_FOUND);
    });


    test('should return 400 if email is invalid', async () => {
      await insertUsers([userOne]);
      const updateBody = { email: 'invalidEmail' };

      await request(app)
        .patch(`/v1/praticiens/${userOne.id}`)
        .set('Authorization', `Bearer ${userOneAccessToken}`)
        .send(updateBody)
        .expect(httpStatus.BAD_REQUEST);
    });

    // test('should return 400 if email is already taken', async () => {
    //   await insertUsers([userOne, userTwo]);
    //   const updateBody = { email: userTwo.email };

    //   await request(app)
    //     .patch(`/v1/praticiens/${userOne.id}`)
    //     .set('Authorization', `Bearer ${userOneAccessToken}`)
    //     .send(updateBody)
    //     .expect(httpStatus.BAD_REQUEST);
    // });

    // test('should not return 400 if email is my email', async () => {
    //   await insertUsers([userOne]);
    //   const updateBody = { email: userOne.email };

    //   await request(app)
    //     .patch(`/v1/praticiens/${userOne._id}`)
    //     .set('Authorization', `Bearer ${userOneAccessToken}`)
    //     .send(updateBody)
    //     .expect(httpStatus.OK);
    // });

    // test('should return 400 if password length is less than 8 characters', async () => {
    //   await insertUsers([userOne]);
    //   const updateBody = { password: 'passwo1' };

    //   await request(app)
    //     .patch(`/v1/praticiens/${userOne._id}`)
    //     .set('Authorization', `Bearer ${userOneAccessToken}`)
    //     .send(updateBody)
    //     .expect(httpStatus.BAD_REQUEST);
    // });

    // test('should return 400 if password does not contain both letters and numbers', async () => {
    //   await insertUsers([userOne]);
    //   const updateBody = { password: 'password' };

    //   await request(app)
    //     .patch(`/v1/praticiens/${userOne._id}`)
    //     .set('Authorization', `Bearer ${userOneAccessToken}`)
    //     .send(updateBody)
    //     .expect(httpStatus.BAD_REQUEST);

    //   updateBody.password = '11111111';

    //   await request(app)
    //     .patch(`/v1/praticiens/${userOne._id}`)
    //     .set('Authorization', `Bearer ${userOneAccessToken}`)
    //     .send(updateBody)
    //     .expect(httpStatus.BAD_REQUEST);
    // });
  });
});

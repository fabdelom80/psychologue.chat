// const { token } = require('morgan');
const Sequelize = require('sequelize');
const models = require('../../models');
const { praticiens, token, abonnements, consultations } = require('../../models');

const config = require('../../src/config/config');

const sequelize = new Sequelize(config.postgres_uri)

const setupTestDB = () => {
  beforeAll(async () => {
    await sequelize.authenticate()
  });

  beforeEach(async () => {
    await consultations.destroy({
      where: {}    })
    await abonnements.destroy({
      where: {}    })
    await token.destroy({
      where: {}    })
    await praticiens.destroy({
      where: {}    })
  });

  afterAll(async () => {
    sequelize.close()
    models.sequelize.close()
  });
};

module.exports = setupTestDB;

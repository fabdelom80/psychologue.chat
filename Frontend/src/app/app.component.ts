import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { LocalStorageService } from './services/local-storage.service';

declare let ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  showCookie;
  isLoggedin;

  constructor(private localStorageService: LocalStorageService, private router: Router, private authService: AuthService) {
  }

  ngAfterViewInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }

  ngOnInit(): void {
    if (!this.localStorageService.get('cookieSeen')) {
      this.localStorageService.set('cookieSeen', false);
      this.showCookie = true;
    } else {
      this.showCookie = false;
    }
  }

  changeCookie(value: boolean) {
    this.localStorageService.set('cookieSeen', value);
    this.showCookie = false;
  }
}
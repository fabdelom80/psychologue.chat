import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutes } from './routes/app.routes';
import { AppMaterialModule } from './app-material.module';
import { SocketioService } from './services/socketio.service';
import { HomeComponent } from './modules/home/home.component';
import { HeaderComponent } from './modules/shared/header/header.component';
import { NotFoundComponent } from './modules/not-found/not-found.component';
import { PaiementComponent } from './modules/paiement/paiement.component';
import { LoginComponent } from './modules/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './modules/register/register.component';
import { ProfilComponent } from './modules/profil/profil.component';
import { WaitingRoomComponent } from './modules/waiting-room/waiting-room.component';
import { AdminModule } from './modules/admin/admin.module';
import { ExportService } from 'src/app/services/export.service';
import { DatePipe } from '@angular/common';

import { PraticienComponent } from './modules/praticien/praticien.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AuthService } from './services/auth.service' 
import { HistoricComponent } from './modules/historic/historic.component';
import { ConfirmdialogComponent } from './modules/confirmdialog/confirmdialog.component';
import { PaiementAbonnementComponent } from './modules/paiement-abonnement/paiement-abonnement.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { ConfirmationGuard } from './guards/confirmation/confirmation.guard';
import { ResetPasswordComponent } from './modules/reset-password/reset-password.component';
import { ToastrModule } from 'ngx-toastr';
import { NotificationService } from './services/notification.service';
import { NgxStripeModule } from 'ngx-stripe';
import { VisioDialogComponent } from './modules/visio-dialog/visio-dialog.component';
import { EchecPaiementComponent } from './modules/echec-paiement/echec-paiement.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    NotFoundComponent,
    PaiementComponent,
    LoginComponent,
    RegisterComponent,
    ProfilComponent,
    WaitingRoomComponent,
    PraticienComponent,
    HistoricComponent,
    ConfirmdialogComponent,
    VisioDialogComponent,
    PaiementAbonnementComponent,
    ResetPasswordComponent,
    EchecPaiementComponent
  ],
  imports: [
    AppRoutes,
    BrowserModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AdminModule,
    ClickOutsideModule,
    ToastrModule.forRoot(),
    NgxStripeModule.forRoot('pk_test_51IHQi4GcXjhA83hbLd2lcklVeLQVP9tTYBUBD4P557AtNpd1uAiWvCzInLe1cJhxrslSc9tZHgSKVAFTJgOLlgRq00Il4zrz9A'),
  ],
  providers: [
    AuthService,
    SocketioService,
    ExportService,
    NotificationService,
    DatePipe,
    ConfirmationGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class VisioGuard implements CanActivate {

  constructor(private localStorage: LocalStorageService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.isAllowed();
  }

  isAllowed(): boolean {
    const page = this.localStorage.get('from');
    if(page) {
      this.localStorage.remove('from');
      return true;
    }
    this.router.navigate(['/home']);
    return false;
  }
  
}

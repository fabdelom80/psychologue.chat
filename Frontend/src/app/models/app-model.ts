

import { EStatutPraticien } from "./enums";

export class Praticien {
    id: number;
    email: string;
    nom: string;
    prenom: string;
    mdp?: string;
    tel: string;
    adresse: string;
    siret: string;
    adeli: string;
    valid: EStatutPraticien;
    online?: boolean;
    role: string;
}

export class Consultation {
    id?: number;
    idpraticien?: number;
    idclient?: number;
    date?: string;
    duree?: number;
}

export class Client {
    id?: number;
    email: string;
    nom: string;
    prenom: string;
}


export class Abonnement {
    id?: number;
    idpraticien?: number;
    datedebut?: string;
    datefin?: string;
}


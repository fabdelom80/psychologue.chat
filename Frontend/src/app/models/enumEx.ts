import { EStatutPraticien } from "./enums";

export class NameValue {
    name: string;
    value: number;
  }
  
  // Classe facilitant l'utilisation des enums
  export class EnumEx {
    private constructor() {
    }
    public static getNamesAndValues<T extends number>(e: any): NameValue[] {
      return EnumEx.getNames(e).map(n => ({ name: n, value: e[n] as T }));
    }
  
    public static getNames(e: any) {
      return Object.keys(e).filter(k => typeof e[k] === 'number') as string[];
    }
  
    public static getValues<T extends number>(e: any) {
      return Object.keys(e)
        .map(k => e[k])
        .filter(v => typeof v === 'number') as T[];
    }
  
    public static toNameArray(arr: NameValue[]) {
      const newArr = [];
      for (const pair of arr) {
        newArr[pair.value] = pair.name;
      }
      return newArr;
    }
  
    public static getNameforValue(nameValue: NameValue[], value: number): string {
      const el = nameValue.find(val => val.value === value);
      if (el) {
        return el.name;
      }
  
      return null;
    }

    
  public static getStatutPraticien(): NameValue[] {
    const types = EnumEx.getNamesAndValues<EStatutPraticien>(EStatutPraticien);
    types[types.findIndex(to => to.name === 'OK')].name = 'Validé';
    types[types.findIndex(to => to.name === 'Attente')].name = 'En attente';
    types[types.findIndex(to => to.name === 'ManqueInformation')].name = 'Manque d\'information';
    types[types.findIndex(to => to.name === 'Refuse')].name = 'Refusé';
    return types;
  }

  }  
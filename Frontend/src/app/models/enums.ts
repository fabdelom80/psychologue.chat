export enum EStatutPraticien {
    Attente = 1,
    ManqueInformation = 2,
    Refuse = 3,
    OK = 4
}

export enum EStatutConnexion {
    EstConnecte = 'Est connecté',
    EstPasConnecte = 'Est pas connecté'
}
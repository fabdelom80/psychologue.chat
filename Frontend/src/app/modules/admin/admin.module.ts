import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { ValidationComponent } from './validation/validation.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { StatsComponent } from './stats/stats.component';
import { RouterModule } from '@angular/router';
import { AppMaterialModule } from 'src/app/app-material.module';

@NgModule({
  declarations: [AdminComponent, ValidationComponent, NavMenuComponent, StatsComponent],
  imports: [CommonModule, RouterModule, AppMaterialModule]
})
export class AdminModule {  }

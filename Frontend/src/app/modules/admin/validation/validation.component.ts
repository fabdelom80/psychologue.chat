import { HttpErrorResponse } from '@angular/common/http';
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, OnInit } from '@angular/core';
import { Praticien } from 'src/app/models/app-model';
import { EnumEx, NameValue } from 'src/app/models/enumEx';
import { EStatutPraticien } from 'src/app/models/enums';
import { NotificationService } from 'src/app/services/notification.service';
import { PraticienService } from 'src/app/services/praticien.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss'],
})

export class ValidationComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }
  
  listeUtilisateur: Praticien[];
  listeUtilisateurFiltered: Praticien[];

  EStatutPraticien = EStatutPraticien;

  listeStatuts: NameValue[];

  statutSelected: EStatutPraticien;

  innerWidth: number = window.innerWidth;

  praticienOpened = 0;

  constructor(private praticienService: PraticienService, private notify: NotificationService) { }

  ngOnInit(): void {
    this.getData();

    this.listeStatuts = EnumEx.getStatutPraticien();

    this.updateListeUtilisateurs();

  }

  getData(statut?: EStatutPraticien) {
    this.praticienService.getAll().subscribe(res => {
      this.listeUtilisateur = res;
      this.listeUtilisateurFiltered = this.listeUtilisateur;
      
      if (statut) {
        this.updateListeUtilisateurs(statut);
      }
    });
  }

  updatePraticien(id: number, statut: EStatutPraticien) {
    let newPraticienStatut: Praticien = new Praticien()
    newPraticienStatut.valid = statut;

    const oldStatut = this.listeUtilisateurFiltered.find(p => p.id === id).valid;
    this.praticienService.update(id, newPraticienStatut).subscribe(() => {
      this.statutSelected ? this.getData(oldStatut) : this.getData();
      this.notify.showSuccess('L\'enregistrement à bien été effectué', 'Succès');
    },
    (err: HttpErrorResponse) => {this.notify.showError(err.error.message, 'Erreur ' + err.error.code); console.log(err)});
  }

  updateListeUtilisateurs(statut?: EStatutPraticien) {
    this.statutSelected = statut;
    this.listeUtilisateurFiltered = statut ? this.listeUtilisateur.filter(p => p.valid == statut) : this.listeUtilisateur;
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EchecPaiementComponent } from './echec-paiement.component';

describe('EchecPaiementComponent', () => {
  let component: EchecPaiementComponent;
  let fixture: ComponentFixture<EchecPaiementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EchecPaiementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EchecPaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {jsPDF} from 'jspdf';
import { Consultation } from 'src/app/models/app-model';
import { ConsultationService } from 'src/app/services/consultation.service';
import { ExportService } from 'src/app/services/export.service';

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.scss']
})


export class HistoricComponent implements OnInit {
  public consultations = [];
  public messageConsultation = '';

  constructor(
    private consultationService: ConsultationService,
    private exportService: ExportService) {
   }

  ngOnInit(): void {
    const idPraticien = localStorage.getItem('idPraticien');
    this.consultationService.getByPraticien(parseInt(idPraticien)).subscribe(rep => {
      this.consultations = rep;
      this.initMessageConsultation();
    });
  }

  @ViewChild('pdfTable', { static: true }) pdfTable: ElementRef;

  
  openPDF(): void {
    const DATA = this.pdfTable.nativeElement;
    const doc: jsPDF = new jsPDF();
    doc.html(DATA, {
       callback: (doc) => {
         doc.output("dataurlnewwindow");
       }
    });
  }

  exportToCsv(): void {
    this.exportService.exportToCsv(this.consultations, 'user-data', ['date', 'date', 'duree']);
  }

  initMessageConsultation(): void {
    if (this.consultations.length < 2) {
      this.messageConsultation = 'Consultation effectuée';
    }
    else {this.messageConsultation = 'Consultations effectuées';}
  }
  }

import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public isLoggedIn;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.isUserLoggedIn.subscribe( value => {
      this.isLoggedIn = value;
      if (value) {
        this.router.navigateByUrl('/praticien');
      }
    });
    this.isLoggedIn = this.authService.isLoggedIn();

    if (this.isLoggedIn) {
      this.router.navigateByUrl('/praticien');
    }
  }

}

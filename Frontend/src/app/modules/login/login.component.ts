import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router';
import { EStatutConnexion } from 'src/app/models/enums';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  connexionForm: FormGroup;
  passwordForm: FormGroup;
  asForgotPassword: boolean = false;
  showWrongLoginAlert = false;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/');
    }

    this.connexionForm =
      this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
      });
    this.passwordForm =
      this.fb.group({
        email: ['', [Validators.required, Validators.email]]
      });
  }

  onSubmit() {
    const val = this.connexionForm.value;

    if (val.email && val.password) {
      this.authService.login(val.email, val.password)
        .subscribe(
          res => {
            this.authService.isUserLoggedIn.subscribe(res => console.log(res));
            this.router.navigateByUrl('/praticien');
            this.showWrongLoginAlert = false;
          },
          (err) => this.showWrongLoginAlert = true
        );
    }
  }

  onSubmitPassword() {
    const val = this.passwordForm.value;

    if (val.email) {
      this.authService.fogotPassword(val.email)
        .subscribe(
          () => {
            console.log("email send");
          }
        );
    }
  }

  get f() { return this.connexionForm.controls; }
  get p() { return this.passwordForm.controls; }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaiementAbonnementComponent } from './paiement-abonnement.component';

describe('PaiementAbonnementComponent', () => {
  let component: PaiementAbonnementComponent;
  let fixture: ComponentFixture<PaiementAbonnementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaiementAbonnementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaiementAbonnementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

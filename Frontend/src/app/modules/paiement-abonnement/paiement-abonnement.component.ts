import { Component, OnInit } from '@angular/core';
import { PaiementService } from 'src/app/services/paiement.service';
import { Router } from "@angular/router";
import { loadStripe, Stripe } from "@stripe/stripe-js";
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-paiement-abonnement',
  templateUrl: './paiement-abonnement.component.html',
  styleUrls: ['./paiement-abonnement.component.scss']
})



export class PaiementAbonnementComponent implements OnInit {

  stripe: Stripe | null = null;
  PUBLISHABLE_KEY: string = "pk_test_51IHQi4GcXjhA83hbLd2lcklVeLQVP9tTYBUBD4P557AtNpd1uAiWvCzInLe1cJhxrslSc9tZHgSKVAFTJgOLlgRq00Il4zrz9A";
  constructor(private paiementService: PaiementService, private router: Router) { }
  private idPraticien = parseInt(localStorage.getItem('idPraticien'));

  async ngOnInit(): Promise<void> {
    this.stripe = await loadStripe(this.PUBLISHABLE_KEY);
  }

  public abo(abo) {
    let route = "aboavie";
    if(abo == 1) route = "abomensuel";
    else if (abo == 2) route = "aboannuel";
    this.paiementService.getToken(route, this.idPraticien).subscribe(rep => {
      this.stripe.redirectToCheckout({
        sessionId: rep
      }).then(function (result) {
      });
    })
  }
}

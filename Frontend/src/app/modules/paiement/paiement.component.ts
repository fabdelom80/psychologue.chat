import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { PaiementService } from 'src/app/services/paiement.service';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {


  constructor(private authService: AuthService, private paiementService: PaiementService, private router: Router, private localStorage: LocalStorageService) {}

  ngOnInit(): void {}

  onSubmit() : void {
    const isStored = this.localStorage.set('from','user');
    if(isStored){
      this.router.navigate(['/visio-room'])
    }
  }
}


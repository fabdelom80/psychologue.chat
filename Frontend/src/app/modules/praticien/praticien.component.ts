import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from "@angular/router";
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Praticien } from '../../models/app-model';
import { PraticienService } from 'src/app/services/praticien.service';
import { EStatutPraticien } from 'src/app/models/enums';

@Component({
  selector: 'app-praticien',
  templateUrl: './praticien.component.html',
  styleUrls: ['./praticien.component.scss']
})
export class PraticienComponent implements OnInit {
  public isLoggedIn;
  public praticien: Praticien = new Praticien();
  public validValue: EStatutPraticien;

  EStatutPraticien = EStatutPraticien;

  constructor(private authService: AuthService, private praticienService: PraticienService,  private router: Router, private localStorage: LocalStorageService) { }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
    if(this.isLoggedIn){
      const idPraticien = localStorage.getItem('idPraticien');
      if(idPraticien){
        this.praticienService.get(parseInt(idPraticien)).subscribe(rep => {
          this.praticien = rep;
          this.validValue = this.praticien.valid;
        });
      }
    }
  }

  redirectToRegister() : void {
    this.router.navigate(['/register'])
  }

  redirectToVisio() : void {
    const isStored = this.localStorage.set('from','praticien');
    if(isStored){
      this.router.navigate(['/visio-room'])
    }
  }

}

/// <reference types="googlemaps" /> 
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ConfirmdialogModel, ConfirmdialogComponent } from '../confirmdialog/confirmdialog.component';
import { MatDialog } from '@angular/material/dialog';
import { PraticienService } from 'src/app/services/praticien.service';
import { Praticien } from '../../models/app-model';
import { FormBuilder, Validators } from '@angular/forms';
import { ConsultationService } from 'src/app/services/consultation.service';
import { AbonnementService } from 'src/app/services/abonnement.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit, AfterViewInit {

  public autocomplete;
  public praticien: Praticien = new Praticien();
  public nbConsultation;
  public subscriptionEnded;
  public dateDebut;
  public dateFin;
  public hasSubscribe = false;
  public checkoutForm = this.formBuilder.group({
    prenom: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    nom: ['', Validators.required],
    tel: ['', Validators.required],
    adresse: ['', Validators.required],
    siret: ['', Validators.required],
    adeli: ['', Validators.required],
  });

  constructor(private router: Router, public dialog: MatDialog, private praticienService: PraticienService, private formBuilder: FormBuilder,
    private consultationService: ConsultationService, private abonnementService: AbonnementService, public datepipe: DatePipe) {
  }

  ngOnInit(): void {
    const idPraticien = localStorage.getItem('idPraticien');
    this.praticienService.get(parseInt(idPraticien)).subscribe(rep => {
      this.praticien = rep;
      this.checkoutForm = this.formBuilder.group({
        prenom: [this.praticien.prenom, Validators.required],
        email: [this.praticien.email, [Validators.required, Validators.email]],
        nom: [this.praticien.nom, Validators.required],
        tel: [this.praticien.tel, Validators.required],
        adresse: [this.praticien.adresse, Validators.required],
        siret: [this.praticien.siret, Validators.required],
        adeli: [this.praticien.adeli, Validators.required],
      });
    });
    this.consultationService.getByPraticien(parseInt(idPraticien)).subscribe(res => {
      this.nbConsultation = res.length;
    })
    this.abonnementService.getLastByPraticien(parseInt(idPraticien)).subscribe((res) => {
      if (res.length > 0) {
        this.hasSubscribe = true;
        this.dateDebut = res[0].datedebut;
        this.dateDebut = this.datepipe.transform(this.dateDebut, 'dd/MM/YYYY');
        this.dateFin = res[0].datefin;
        let endDate = new Date(this.dateFin);
        this.dateFin = this.datepipe.transform(this.dateFin, 'dd/MM/YYYY');
        let today = new Date();
        var diff = endDate.getTime() - today.getTime();
        var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
        if (diffDays <= 0) {
          this.subscriptionEnded = 1;
        } else if (diffDays <= 7) {
          this.subscriptionEnded = 2;
        } else if (endDate.getFullYear() >= 2121) {
          this.subscriptionEnded = 3;
        } else {
          this.subscriptionEnded = 4;
        }
      }
    });
  }

  ngAfterViewInit() {
    this.initAutocomplete();
  }

  private initAutocomplete() {
    this.autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete') as HTMLInputElement,
      {
        types: ["geocode"],
        componentRestrictions: { 'country': ['FR'] }
      });

    this.autocomplete.setFields(["address_component"]);

    this.autocomplete.addListener('place_changed', () => {
      var place = this.autocomplete.getPlace();
      this.checkoutForm.value.adresse = place.address_components[0].long_name + ", " + place.address_components[1].long_name + ", " + place.address_components[2].long_name + ", " + place.address_components[5].long_name;
    });
  }

  redirectToPaiement(): void {
    this.router.navigate(['/paiement-abonnement'])
  }

  redirectToHistoric(): void {
    this.router.navigate(['/historique'])
  }

  confirmDialog(): void {
    const dialogData = new ConfirmdialogModel();
    const dialogRef = this.dialog.open(ConfirmdialogComponent, {
      maxWidth: "400px"
    });
  }

  onSubmit(): void {
    // Process checkout data here
    this.praticien = this.checkoutForm.value;
    if (this.checkoutForm.valid) {
      console.log(this.checkoutForm.value);
      this.praticienService.update(parseInt(localStorage.getItem('idPraticien')), this.checkoutForm.value).subscribe(rep => { 
      });
    }
  }
  get f() { return this.checkoutForm.controls; }

  
}

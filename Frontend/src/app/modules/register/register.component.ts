/// <reference types="googlemaps" />
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Praticien } from 'src/app/models/app-model';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { PraticienService } from 'src/app/services/praticien.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterViewInit {

  public autocomplete;
  registerForm: FormGroup;
  scorePwd = 0;
  errorPwdNonIdentical = false;
  passwordConfirmedEntered = false;
  loading = false;
  adresseGoogle;
  newPraticien = new Praticien();
  showConfirmationMessage = false;
  emailUse;
  adeliUse;
  siretUse;

  constructor(private fb: FormBuilder, 
    private praticienService: PraticienService, 
    private authService: AuthService, 
    private router: Router,
    private notify: NotificationService) { }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/home');
    }

    this.registerForm =
      this.fb.group({
        nom: ['', Validators.required],
        prenom: ['', Validators.required],
        tel: ['', Validators.required],
        adresse: ['', Validators.required],
        siret: ['', Validators.required],
        adeli: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        passwordConfirm: ['', Validators.required]
      });
  }

  ngAfterViewInit(): void {
    this.initAutocomplete();
  }

  private initAutocomplete() {
    this.autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete') as HTMLInputElement,
      {
        types: ["geocode"],
        componentRestrictions: {'country': ['FR']}
      });

      this.autocomplete.setFields(["address_component"]);
    
      this.autocomplete.addListener('place_changed', () => {
        var place = this.autocomplete.getPlace();
        this.adresseGoogle = place.address_components[0].long_name + ", " + place.address_components[1].long_name + ", " + place.address_components[2].long_name + ", " + place.address_components[5].long_name;
      });
  }

  onSubmit() {
    this.loading = true;
    if (this.registerForm.valid) {
      this.newPraticien.email = this.f.email.value;
      this.newPraticien.nom = this.f.nom.value;
      this.newPraticien.prenom = this.f.prenom.value;
      this.newPraticien.tel = this.f.tel.value;
      this.newPraticien.adresse = this.adresseGoogle;
      this.newPraticien.adeli = this.f.adeli.value;
      this.newPraticien.mdp = this.f.password.value;
      this.newPraticien.siret = this.f.siret.value;

      this.praticienService.create(this.newPraticien).subscribe(() => {
        this.loading = false;
        this.registerForm.reset();
        this.scorePwd = 0;

        this.showConfirmationMessage = true;
      });
    }
  }

  get f() { return this.registerForm.controls; }

  getStrengthPassword() {
    let score = 0;
    let letters = {};

    const password = this.f.password.value;
    for (let i = 0; i < password.length; i++) {
      letters[password[i]] = (letters[password[i]] || 0) + 1;
      score += 5.0 / letters[password[i]];
    }
    let variations = {
      digits: /\d/.test(password),
      lower: /[a-z]/.test(password),

      upper: /[A-Z]/.test(password),

      nonWords: /\W/.test(password),
    };

    let variationCount = 0;
    for (let check in variations) {
      variationCount += (variations[check]) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;
    this.scorePwd = Math.trunc(score);
  }

  checkPwd() {
    this.errorPwdNonIdentical = this.f.password.value != this.f.passwordConfirm.value;
  }

  public checkEmailUse(target: any) {
    this.emailUse =false;
    if(target.value.includes('@')) {
      var firstPart = target.value.split('@');
      if (firstPart[1].includes('.')) {
        var twoPart = firstPart[1].split('.');
        if(twoPart[1].length>1){
          this.praticienService.getByEmailBoolean(target.value).subscribe(rep => {
            this.emailUse = rep;});
         }
      }
    }
}

public checkAdeliUse(target: any) {
  this.adeliUse =false;
  if(target.value.length == 9) {
        this.praticienService.getByAdeli(target.value).subscribe(rep => {
          this.adeliUse = rep;});
       }
}

public checkSiretUse(target: any) {
  this.siretUse =false;
  if(target.value.length == 14) {
        this.praticienService.getBySiret(target.value).subscribe(rep => {
          this.siretUse = rep;});
       }
}

}

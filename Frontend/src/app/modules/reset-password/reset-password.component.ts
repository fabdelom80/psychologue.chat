import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  newPasswordForm: FormGroup;
  loading = false;
  scorePwd = 0;
  errorPwdNonIdentical = false;
  passwordConfirmedEntered = false;
  token = null;
  messageError = null;
  showConfirmationMessage = false;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.newPasswordForm =
    this.fb.group({
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required]
    });
    this.route.queryParamMap.subscribe(params => {
      this.token = params.get('token');
      if (!this.token) {
        this.router.navigate(['/404']);
      }
    });
  }

  onSubmit() {
    this.loading = true;

    if (this.newPasswordForm.valid) {
      this.authService.resetPassword(this.token,this.f.password.value)
        .subscribe(
          result => {},
          error => {
            this.loading = false;
            this.messageError = error.error.message;
          },
          () => {
            this.showConfirmationMessage = true;
          }
        );
    }
  }

  getStrengthPassword() {
    let score = 0;
    let letters = {};

    const password = this.f.password.value;
    for (let i = 0; i < password.length; i++) {
      letters[password[i]] = (letters[password[i]] || 0) + 1;
      score += 5.0 / letters[password[i]];
    }
    let variations = {
      digits: /\d/.test(password),
      lower: /[a-z]/.test(password),

      upper: /[A-Z]/.test(password),

      nonWords: /\W/.test(password),
    };

    let variationCount = 0;
    for (let check in variations) {
      variationCount += (variations[check]) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;
    this.scorePwd = Math.trunc(score);
  }


  checkPwd() {
    this.errorPwdNonIdentical = this.f.password.value != this.f.passwordConfirm.value;
  }

  get f() { return this.newPasswordForm.controls; }

}

import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ADMIN_ROLE, PRATICIEN_ADMIN_ROLE } from 'src/app/constantes';
import { EStatutPraticien } from 'src/app/models/enums';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { PraticienService } from 'src/app/services/praticien.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public isLoggedIn;
  public isOnPraticienPage;
  public isOnVisioPage;
  public showMenuCompte;
  public showMenuPhone;
  public role;
  public validValue = false;

  EStatutPraticien = EStatutPraticien;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  public innerWidth = 1001;

  constructor(private authService: AuthService, private router: Router, private localStorage: LocalStorageService, private praticienService: PraticienService) { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;

    this.authService.isUserLoggedIn.subscribe(value => {
      this.isLoggedIn = value;
    });


    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isOnPraticienPage = event.url === '/praticien';
        this.isOnVisioPage = event.url === '/visio-room';
      }
    });

    this.isLoggedIn = this.authService.isLoggedIn();
  }

  logout(): void {
    this.authService.logout();
  }

  handleClickOutside() {
    if (this.showMenuCompte) {
      this.showMenuCompte = false;
    }
  }

  redirectToVisio(): void {
    const isStored = this.localStorage.set('from', 'praticien');
    if (isStored) {
      this.router.navigate(['/visio-room'])
    }
  }

  isAdmin(): boolean {
    return this.role == PRATICIEN_ADMIN_ROLE || this.role == ADMIN_ROLE;
  }
}

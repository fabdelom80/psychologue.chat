import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisioDialogComponent } from './visio-dialog.component';

describe('VisioDialogComponent', () => {
  let component: VisioDialogComponent;
  let fixture: ComponentFixture<VisioDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisioDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisioDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

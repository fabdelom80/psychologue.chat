import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SocketioService } from 'src/app/services/socketio.service';

@Component({
  selector: 'app-visio-dialog',
  templateUrl: './visio-dialog.component.html',
  styleUrls: ['./visio-dialog.component.scss']
})
export class VisioDialogComponent implements OnInit {


  isWaitingAnswer: boolean = false;
  isPraticien: boolean = false;

  constructor(public dialogRef: MatDialogRef<VisioDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: { socket: SocketioService, isPraticien: boolean }) { }

  ngOnInit(): void {
    this.isPraticien = this.data.isPraticien;
    if (this.isPraticien) {
      this.isWaitingAnswer = true;
      this.data.socket.listen('user_ask_pra', (isOk) => {
        this.isWaitingAnswer = false;
        if(!isOk){
          this.dialogRef.close(isOk);
        }
      });
    } else {
      this.data.socket.listen('pra_answer_user', (isOk) => {
        this.dialogRef.close(isOk);
      });
    }
  }

  askPraticien(isOk) {
    this.data.socket.send('user_ask_visio', isOk);
    if(!isOk){
      this.dialogRef.close(isOk);
    } else {
      this.isWaitingAnswer = true;
    }
  }

  answerUser(isOk) {
    this.data.socket.send('pra_answer_visio', isOk);
    this.dialogRef.close(isOk);
  }
}
export class VisioDialogModel {

  constructor() {
  }
}


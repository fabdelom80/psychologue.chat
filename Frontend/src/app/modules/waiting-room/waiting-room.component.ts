import { HostListener, Renderer2 } from '@angular/core';
import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { SocketioService } from 'src/app/services/socketio.service';
import { CanComponentDeactivate } from '../../guards/confirmation/confirmation.guard';
import SimplePeer from 'simple-peer';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { PRATICIEN_ADMIN_ROLE, PRATICIEN_ROLE } from 'src/app/constantes';
import { MatDialog } from '@angular/material/dialog';
import { VisioDialogComponent, VisioDialogModel } from '../visio-dialog/visio-dialog.component';

const PEER_CONNECTION_CONFIG: RTCConfiguration = {
  iceServers: [{
    urls: ["stun.l.google.com:19302", "stun1.l.google.com:19302", "stun2.l.google.com:19302", "stun3.l.google.com:19302", "stun4.l.google.com:19302"]
  }]
};

@Component({
  selector: 'app-waiting-room',
  templateUrl: './waiting-room.component.html',
  styleUrls: ['./waiting-room.component.scss']
})

export class WaitingRoomComponent implements OnInit, CanComponentDeactivate {

  @ViewChild('videostream') video_stream: ElementRef<HTMLVideoElement>;
  @ViewChild('remoteVideo') video_received: ElementRef<HTMLVideoElement>;

  constructor(private authService: AuthService,public dialog: MatDialog, private socketService: SocketioService, private _renderer: Renderer2, private router: Router) {
  }

  private isLoggedIn: boolean = false;
  isWaitingConfirmation = true;
  video = null;
  remoteVideo = null;
  roomId = null;
  remoteUserDisconnected = false;
  private peer1: SimplePeer = null;
  private peer2: SimplePeer = null;
  showDivWait = true;
  showDivStart = false;
  time = "";
  mutedButton = false; //false : non mute (bouton vert) - true : mute (bouton rouge)
  color = "black"; // couleur timer : noir de 30:00 à 05:00 puis rouge de 05:00 à 00:00
  myMicro = null;
  timerInterval = null; //variable timer
  isPraticien = false;

  ngOnInit(): void {
    this.authService.isUserLoggedIn.subscribe(value => {
      this.isLoggedIn = value;
    });

    this.isLoggedIn = this.authService.isLoggedIn();
    this.roomId = this.createUuid();
    window.addEventListener("beforeunload", this.closeEvent, true);

    //role : string
    switch (this.authService.getRole()) {
      case PRATICIEN_ROLE:
      case PRATICIEN_ADMIN_ROLE:
        this.isPraticien = true;
        break;
      default:
        this.isPraticien = false;
        break;
    }

    this.socketEmit();
  }

  ngAfterViewInit() {
    this.video = this.video_stream.nativeElement;
    this.remoteVideo = this.video_received.nativeElement;
  }

  ngOnDestroy() {
    try {
      this.socketService.disconnect();
      this.dialog.closeAll();
    } catch (error) {
      console.error(error);
    }
    this.clearPeers();
    window.removeEventListener("beforeunload", this.closeEvent, true);
    this.destroyVideoStream(this.video);
    this.destroyVideoStream(this.remoteVideo);
    clearInterval(this.timerInterval);
  }

  socketEmit() {
    this.socketService.setupSocketConnection();
    this.socketService.onConnect(() => {
      if (this.isPraticien) {
        this.socketService.send('praticien_start', { id: this.socketService.socketId, roomId: this.roomId });
      } else {
        this.socketService.send('user_start', { id: this.socketService.socketId });
      }

      this.socketService.listen('room_found', (roomId) => {
        this.roomId = roomId;
        this.showDivWait = false;
        this.showDivStart = true;
        this.getMedia();
      })

      this.socketService.listen('room_left',(user)=>{
        this.remoteUserDisconnected = true;
        alert('Votre interlocuteur a quitté la vidéo-conférence, vous allez être redirigé vers la page d\'accueil');
        this.router.navigateByUrl('/home');
      });
    })

  }


  getMedia() {
    navigator.mediaDevices.getUserMedia({
      video: true,
      audio: true
    }).then((stream) => {
      try {

        this.confirmDialog();

        this.myMicro = stream.getAudioTracks()[0];

        this.video.srcObject = stream;

        this.socketService.requestForJoiningRoom({ roomId: this.roomId })

        this.socketService.onRoomParticipants(calleeId => {
          this.initilizePeersAsCaller(calleeId, stream)
        })

        this.socketService.onOffer(msg => {
          this.initilizePeersAsCallee(msg, stream)
        })

        this.socketService.onAnswer(msg => {
          this.peer1.signal(msg.signalData)
        })

      }
      catch (error) {
        console.error("initialisation error");
      }
    }).catch((loadFail) => {
      console.error(loadFail);
    });
  }

  initilizePeersAsCaller(calleeId: string, stream: MediaStream) {
    this.peer1 = new SimplePeer({ initiator: true, trickle: false, stream: stream })

    this.peer1.on('signal', signal => {
      this.socketService.sendOfferSignal({ signalData: signal, calleeId: calleeId })
    })

    this.peer1.on('stream', stream => {
      this.remoteVideo.srcObject = stream
    })
  }

  initilizePeersAsCallee(msg: any, stream: MediaStream) {
    this.peer2 = new SimplePeer({
      initiator: false,
      trickle: false,
      stream
    });

    this.peer2.on('signal', signal => {
      this.socketService.sendAnswerSignal({ signalData: signal, callerId: msg.callerId })
    })

    this.peer2.on('stream', stream => {
      this.remoteVideo.srcObject = stream;
    })

    this.peer2.signal(msg.signalData)
  }


  private createUuid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  private destroyVideoStream(videoFrame) {
    try {
      videoFrame.srcObject.getTracks().forEach(element => {
        element.stop();
      });
    } catch (error) {
      console.error(error);
    }
    videoFrame.srcObject = null;
  }


  confirm() {
    return true;
    //return confirm("Êtes-vous sûr de vouloir quitter cette page ? Cette action est irréversible.");
  }

  confirmDialog(): void {
    const dialogData = new VisioDialogModel();
    const dialogRef = this.dialog.open(VisioDialogComponent, {
      maxWidth: "400px",
      disableClose: true,
      data: {
        isPraticien: this.isPraticien,
        roomId: this.roomId,
        socket: this.socketService
      }
    })
    dialogRef.beforeClosed().subscribe((isValidated) => {
      if( isValidated ) {
        this.timer();
      } else {
        window.removeEventListener("beforeunload", this.closeEvent, true);
        this.router.navigateByUrl('/home');
      }
    })
  }


  timer() {

    const TIME_LIMIT = 1800; //en secondes
    let timePassed = -1;
    let timeLeft = TIME_LIMIT;
    //let timerInterval = null;

    this.timerInterval = setInterval(() => {
      timePassed = timePassed += 1;
      timeLeft = TIME_LIMIT - timePassed;

      if (timeLeft <= 300) {
        this.color = "red";
      }

      this.time = formatTime(timeLeft);

      if (timeLeft === 0) {
        this.video_received.nativeElement.muted;
        alert('La téléconsultation est terminée, merci.')
        this.router.navigateByUrl('/home');
        clearInterval(this.timerInterval);
      }

    }, 1000);

    function formatTime(time) {
      const minutes = Math.floor(time / 60);
      let seconds = time % 60;

      if (seconds < 10) {
        return `${minutes}:0${seconds}`;;
      }

      return `${minutes}:${seconds}`;
    }

  }

  mute() {
    if (this.mutedButton === true) {
      this.myMicro.enabled = true;
      this.mutedButton = false;

    } else {
      this.myMicro.enabled = false;
      this.mutedButton = true;
    }
  }

  stop() {
    if (confirm("Vous êtes sur le point de quitter la vidéo-conférence, confirmez-vous cette action ?")) {
      this.router.navigateByUrl('/home');
    }
  }

  closeEvent(event) {
    event.preventDefault();
    event.returnValue = "Unsaved modifications";
    return event;
  }

  clearPeers() {
    try {
      this.peer1.peerConnection.dispose();
      this.peer1.videoSource.dispose();
      this.peer1.peerConnectionFactory.dispose();
      this.peer1.destroy(error => console.error("no peer 1"));
    } catch(error){
    }
    try{
      this.peer2.peerConnection.dispose();
      this.peer2.videoSource.dispose();
      this.peer2.peerConnectionFactory.dispose();
      this.peer2.destroy(error => console.error("no peer 2"));
    } catch(error) {
    }
  }


}
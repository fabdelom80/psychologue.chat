import { StatsComponent } from "../modules/admin/stats/stats.component";
import { ValidationComponent } from "../modules/admin/validation/validation.component";

export const adminRoutes = [
    {
        path: '',
        redirectTo: 'validation',
        pathMatch: 'full'
    },
    {
        path: 'validation',
        component: ValidationComponent,
    },
    {
        path: 'stats',
        component: StatsComponent,
    },
];
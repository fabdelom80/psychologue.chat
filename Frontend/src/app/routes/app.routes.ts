import { ModuleWithProviders } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "../modules/home/home.component";
import { PaiementComponent } from "../modules/paiement/paiement.component";
import { LoginComponent } from "../modules/login/login.component";
import { NotFoundComponent } from "../modules/not-found/not-found.component";
import { RegisterComponent } from "../modules/register/register.component";
import { ProfilComponent } from "../modules/profil/profil.component";
import { WaitingRoomComponent } from "../modules/waiting-room/waiting-room.component";
import { AdminComponent } from "../modules/admin/admin.component";
import { adminRoutes } from "./admin.routes";
import { PraticienComponent } from "../modules/praticien/praticien.component";
import { HistoricComponent } from "../modules/historic/historic.component";
import { PaiementAbonnementComponent } from '../modules/paiement-abonnement/paiement-abonnement.component';
import { ConfirmationGuard } from "../guards/confirmation/confirmation.guard";
import { ResetPasswordComponent } from "../modules/reset-password/reset-password.component";
import { AuthGuard } from "../guards/confirmation/auth.guard";
import { ADMIN_ROLE, PRATICIEN_ADMIN_ROLE, PRATICIEN_ROLE } from "../constantes";
import { VisioGuard } from "../guards/visio.guard";
import { HomeGuard } from "../guards/home.guard";
import { EchecPaiementComponent } from "../modules/echec-paiement/echec-paiement.component";
export const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent,
        pathMatch: 'full',
    },
    {
        path: 'historique',
        component: HistoricComponent,
        pathMatch: 'full',
        canActivate: [AuthGuard],
        data: {
            role: [PRATICIEN_ADMIN_ROLE, PRATICIEN_ROLE]
        }
    },
    {
        path: 'paiement',
        component: PaiementComponent,
        pathMatch: 'full'
    },
    {
        path: 'echec-paiement',
        component: EchecPaiementComponent,
        pathMatch: 'full'
    },
    {
        path: 'paiement-abonnement',
        component: PaiementAbonnementComponent,
        pathMatch: 'full',
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent,
        pathMatch: 'full',
    },
    {
        path: 'register',
        component: RegisterComponent,
        pathMatch: 'full',
    },
    {
        path: 'profil',
        component: ProfilComponent,
        pathMatch: 'full',
        canActivate: [AuthGuard],
        data: {
            role: [PRATICIEN_ADMIN_ROLE, PRATICIEN_ROLE]
        }
    },
    {
        path: 'admin',
        component: AdminComponent,
        children: adminRoutes,
        canActivate: [AuthGuard],
        data: {
            role: [ADMIN_ROLE, PRATICIEN_ADMIN_ROLE]
        }
    },
    {
        path: 'visio-room',
        component: WaitingRoomComponent,
        canDeactivate: [ConfirmationGuard],
        canActivate: [VisioGuard],
        pathMatch: 'full'
    },
    {
        path: 'praticien',
        component: PraticienComponent,
        pathMatch: 'full'
    },
    {
        path: 'reset-password',
        component: ResetPasswordComponent
    },
    {
        path: '404',
        component: NotFoundComponent
    },
    {
        path: "**",
        redirectTo: '/404'
    }
];

export const AppRoutes: ModuleWithProviders<any> = RouterModule.forRoot(routes);

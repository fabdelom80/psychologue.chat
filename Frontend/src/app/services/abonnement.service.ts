import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Abonnement } from '../models/app-model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';


const baseUrl = environment.API_URL + 'abonnements';


@Injectable({
  providedIn: 'root'
})
export class AbonnementService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Abonnement[]> {
    return this.http.get<Abonnement[]>(baseUrl);
  }

  get(id: number): Observable<Abonnement> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: Abonnement): Observable<Abonnement> {
    return this.http.post(baseUrl, data);
  }

  update(id: number, data: Abonnement): Observable<any> {
    return this.http.patch(`${baseUrl}/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  getByPraticien(idpraticien: number): Observable<Abonnement[]> {
    return this.http.get(`${baseUrl}/praticien/${idpraticien}`).pipe(
      map((res: Abonnement[]) => res));
  }

  getLastByPraticien(idpraticien: number): Observable<Abonnement[]> {
    return this.http.get(`${baseUrl}/last/praticien/${idpraticien}`).pipe(
      map((res: Abonnement[]) => res));
  }
}
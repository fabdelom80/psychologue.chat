import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Administrateur } from '../models/app-model';
import { environment } from '../../environments/environment';


const baseUrl = environment.API_URL + 'administrateurs';


@Injectable({
  providedIn: 'root'
})
export class AdministrateurService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Administrateur[]> {
    return this.http.get<Administrateur[]>(baseUrl);
  }

  get(id: number): Observable<Administrateur> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: Administrateur): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: number, data: Administrateur): Observable<any> {
    return this.http.patch(`${baseUrl}/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  getByEmail(email: string): Observable<Administrateur> {
    return this.http.get(`${baseUrl}/${email}`);
  }
}
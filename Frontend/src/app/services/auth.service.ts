import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { share, shareReplay, tap } from 'rxjs/operators'
import { BehaviorSubject, Observable, Subject } from 'rxjs'
import { Praticien } from '../models/app-model';
import { environment } from '../../environments/environment';
import * as moment from "moment";
import { Router } from '@angular/router';
import { EStatutConnexion, EStatutPraticien } from '../models/enums';

const baseUrl = environment.API_URL + 'auth';


@Injectable()
export class AuthService {
  public isUserLoggedIn: Subject<boolean> = new Subject<boolean>();
  public statutPraticien: Subject<number> = new Subject<number>();

  private role: string;

  constructor(private http: HttpClient, private router: Router) {}

  login(email: string, password: string) {
    return this.http.post<Praticien>(`${baseUrl}/login`, { email, password })
      .pipe(
        tap(res => {
          this.setSession(res);
        }),
        shareReplay())
  }

  private setSession(authResult) {
    this.isUserLoggedIn.next(true);

    localStorage.setItem('idPraticien', authResult.user.id);
    localStorage.setItem('role', authResult.user.role);
    localStorage.setItem('token', authResult.tokens.access.token);
    localStorage.setItem('refreshToken', authResult.tokens.refresh.token);
    localStorage.setItem("expires", authResult.tokens.access.expires);
  }

  logout() {
    this.isUserLoggedIn.next(false);

    localStorage.removeItem("token");
    localStorage.removeItem("refreshToken");
    localStorage.removeItem("expires");
    localStorage.removeItem("idPraticien");
    localStorage.removeItem("role");

    this.router.navigateByUrl('/home');
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem("expires");
    return moment(expiration);
  }

  getRole() {
    this.role = localStorage.getItem("role");
    return this.role;
  }

  refreshToken(): Observable<any> {
    const refreshToken = localStorage.getItem('refreshToken');
    const accessToken = localStorage.getItem('token');
    return this.http.post<{accessToken: string; refreshToken: string}>(
      baseUrl + '/refresh-tokens',
      {
        refreshToken
      }).pipe(
        tap(response => {
          console.log(response.access.token)
          console.log(response.refresh.token)

          this.setToken('token', response.access.token);
          this.setToken('refreshToken', response.refresh.token);
        })
    );
  }

  private setToken(key: string, token: string): void {
    localStorage.setItem(key, token);
  }

  fogotPassword(email: string): Observable<any> {
    return this.http.post(baseUrl + '/forgot-password', { email: email });
  }

  resetPassword(token: string, password: string): Observable<any> {
    return this.http.post(baseUrl + '/reset-password', { password: password }, { params: { "token": token } });
  }

}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Client } from '../models/app-model';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private baseUrl = environment.API_URL + 'clients';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Client[]> {
    return this.http.get<Client[]>(this.baseUrl).pipe(
      map((res: Client[]) => res));
  }

  get(id: number): Observable<Client> {
    return this.http.get(`${this.baseUrl}/${id}`).pipe(
      map((res: Client) => res));
  }

  create(data: Client): Observable<any> {
    return this.http.post(this.baseUrl, data);
  }

/*   Pas OK  côté back 
    update(id: number, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  } */

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

}
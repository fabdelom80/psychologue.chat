import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Consultation } from '../models/app-model';
import { environment } from '../../environments/environment';


const baseUrl = environment.API_URL + 'consultations';


@Injectable({
  providedIn: 'root'
})
export class ConsultationService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Consultation[]> {
    return this.http.get<Consultation[]>(baseUrl);
  }

  get(id: number): Observable<Consultation> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: Consultation): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: number, data: Consultation): Observable<any> {
    return this.http.patch(`${baseUrl}/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  getByClient(idclient: number): Observable<Consultation[]> {
    return this.http.get<Consultation[]>(`${baseUrl}/client/${idclient}`);
  }

  getByPraticien(idpraticien: number): Observable<Consultation[]> {
    return this.http.get<Consultation[]>(`${baseUrl}/praticien/${idpraticien}`);
  }

}
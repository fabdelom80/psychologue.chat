import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';


const baseUrl = environment.API_URL + 'demandepaiement';


@Injectable({
  providedIn: 'root'
})
export class PaiementService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(baseUrl);
  }

  get(id: number): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  getToken(route: string, id: number): Observable<any> {
    return this.http.get(`${baseUrl}/${route}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  getByPraticien(idpraticien: number): Observable<any[]> {
    return this.http.get(`${baseUrl}/praticien/${idpraticien}`).pipe(
      map((res: any[]) => res));
  }

}
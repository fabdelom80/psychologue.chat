import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Praticien } from '../models/app-model';
import { environment } from '../../environments/environment';


const baseUrl = environment.API_URL + 'praticiens';


@Injectable({
  providedIn: 'root'
})
export class PraticienService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Praticien[]> {
    return this.http.get<Praticien[]>(baseUrl);
  }

  get(id: number): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: Praticien): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: number, data: any): Observable<any> {
    return this.http.patch(`${baseUrl}/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  // valid = 1 ==> Renvoie les praticien en attente    / Valid = 4 ==> Renvoie les praticiens valid
  getValid(valid: number): Observable<Praticien[]> {
    return this.http.get<Praticien[]>(`${baseUrl}/valid/${valid}`);
  }

  getValidAndOnline(): Observable<Praticien[]> {
    return this.http.get<Praticien[]>(`${baseUrl}/available`);
  }

  getByEmail(email: string): Observable<Praticien> {
    return this.http.get<Praticien>(`${baseUrl}/email/${email}`);
  }

  getByEmailBoolean(email: string): Observable<Boolean> {
    return this.http.get<boolean>(`${baseUrl}/emailboolean/${email}`);
  }

  getByAdeli(adeli: string): Observable<Boolean> {
    return this.http.get<boolean>(`${baseUrl}/adeli/${adeli}`);
  }

  getBySiret(siret: string): Observable<Boolean> {
    return this.http.get<boolean>(`${baseUrl}/siret/${siret}`);
  }



}
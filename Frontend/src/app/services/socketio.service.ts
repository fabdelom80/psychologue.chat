import { Injectable } from '@angular/core';
import { io } from 'socket.io-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketioService {

  private socket;
  constructor() {   }


  setupSocketConnection() {
    this.socket = io(environment.SOCKET_ENDPOINT);
  }

  get socketId() {
    return this.socket.id
  }

  
  listen(channel: string, fn: Function) {
    this.socket.on(channel, fn)
  }

  send(chanel: string, message: any) {
    this.socket.emit(chanel, message)
  }

  onConnect(fn: () => void) {
    this.listen('connect', fn)
  }

  requestForJoiningRoom(msg: any) {
    this.send('room_join_request', msg)
  }

  onRoomParticipants(fn: (participants: string) => void) {
    this.listen('room_users', fn)
  }

  sendOfferSignal(msg: any) {
    this.send('offer_signal', msg)
  }

  onOffer(fn: (msg: any) => void) {
    this.listen('offer', fn)
  }

  sendAnswerSignal(msg: any) {
    this.send('answer_signal', msg)
  }

  onAnswer(fn: (msg: any) => void) {
    this.listen('answer', fn)
  }

  onRoomLeft(fn: (socketId: string) => void) {
    this.listen('room_left', fn)
  }

  disconnect(){
    this.socket.disconnect();
  }
}

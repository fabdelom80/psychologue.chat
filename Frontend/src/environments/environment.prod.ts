export const environment = {
  production: true,
  SOCKET_ENDPOINT: 'https://visio.psychologue.chat/',
  API_URL: 'https://api.psychologue.chat/v1/'
};
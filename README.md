# Mise en place de l'environnement de developpement


## Frontend 

### installation angular

```bash
npm install -g @angular/cli

cd Frontend

npm install

ng serve
```



## Backend

### Installation node js

https://nodejs.org/en/ version 14.15.1

```bash
npm install --global yarn
```

```bash
cd Backend
```

```bash
npx rimraf ./.git
```

Install the dependencies:

```bash
yarn install
```

Set the environment variables:

```bash
cp .env.example .env

# open .env and modify the environment variables (if needed)
#ajouter uri postgres

POSTGRES_URI=postgres://<user>:<mdp>@localhost:5432/<db_name>
URL_API=http://127.0.0.1:3000/
URL_FRONT=http://127.0.0.1:4200/
```

## Génération de la documentation en Front-end (Angular)
```npm run compodoc```
Ne pas add les fichiers générés dans Git

## Visio

```bash
cd Visio/
cp .env.example .env
npm install
```

## Commands

Running locally:

```bash
yarn dev
```

Running in production:

```bash
yarn start
```

------------------

# Architecture du Frontend

### Angular Material
Module à part pour Material -> tous les modules material (comme MatInputModule par exemple) doivent être importés dans app-material.module.ts et non dans le app.module.ts.
# Backend instructions

### Dossiers de l'archi :
- **guard**: dossier où seront mis les guard (AuthGuard par exemple)
- **helpers**: dossier où seront mis les interceptors
- **models**: dossiers où seront mis tous les models et les enums, pour l'instant seul app-model.ts existe (utilisé pour les models les plus génériques) mais plusieurs fichiers de models peuvent et doivent être créés pour que ce soit plus clair dans l'appli.
- **modules**: emplacement de tous les composants de l'application. Dans le dossier shared doivent être placés les composants utilisés plusieurs fois dans l'appli.
- **routes**: emplacement des fichiers de routing
- **services**: emplacement des services
- **assets** : lorsque des images ont besoin d'être directement sur le serveur, comme par exemple le logo de l'application.
- **environments**: contient les variables d'environnements
- **constantes.ts** : fichier contenant toutes les constantes de l'application
- **variables**: les fichiers scss globaux.
1. colors.scss : TOUTES les couleurs utilisées dans le css de l'appli doivent provenir de ce fichier. C'est-à-dire que dans tous les autres fichiers scss de l'application qui ont une couleur doivent importer (@import "../../../../variables/colors";) avec la variable de couleur dedans.
2. fonts.scss : fichier pour importer les polices d'écriture
3. variables.scss : les variables globales de scss autre que les polices et les couleurs.

## Kubernetes

### Création des secrets

Activer istio sur le cluster

```bash
kubectl create secret generic -n therapeutholib database-secret --from-literal=db_username=<user> --from-literal=db_password=<mot de passe>
npx rimraf ./.git
```

kubectl create secret generic -n therapeutholib service-account-cloudsql --from-file=service_account.json
Install the dependencies:

kubectl apply -f Ops/k8s-istio.yml -n therapeutholib
```bash
yarn install
```

### Generation des certificats

installer letsencrypt
```bash
sudo certbot certonly --manual --preferred-challenges=dns --email psychologue.click@gmail.com --server https://acme-v02.api.letsencrypt.org/directory -d "*.psychologue.chat"
```


Set the environment variables:

## BDD

### Se connecter avec le proxy Cloud SQL
```bash
cp .env.example .env

Installer le proxy Cloud SQL  https://cloud.google.com/sql/docs/postgres/connect-admin-proxy#install-the-cloud-sql-proxy
# open .env and modify the environment variables (if needed)
```

Téléchargez le ["CREDENTIAL FILES"](https://trello-attachments.s3.amazonaws.com/6017c3c4341a62495d6fc6ce/6017c7caa7adbc37daf7505f/8187a87195764415b13bdbb991e8b084/halogen-antenna-300416-6f895790a057.json) et le placer dans le même dossier que le proxy Cloud SQL
## Commands

Ouvrir la console, se placer dans le dossier contenant les fichiers précédents et executer la commande suivante : 
Running locally:

```bash
cloud_sql_proxy.exe -instances=halogen-antenna-300416:europe-west1:postgres-psychologue-click-clone=tcp:5432 -credential_file=halogen-antenna-300416-6f895790a057.json &
psql "host=127.0.0.1 sslmode=disable dbname=psychologue-test user=postgres"

# Backend instructions



```bash
npx rimraf ./.git
```

Install the dependencies:

```bash
yarn install
```

Set the environment variables:

```bash
cp .env.example .env

# open .env and modify the environment variables (if needed)
```

## Commands

Running locally:

```bash
yarn dev
```

Running in production:

```bash
yarn start
```


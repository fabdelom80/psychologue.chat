const express = require('express');
require('dotenv').config()
const app = express();

const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

require('./socket/socket')(io);

server.listen(process.env.PORT, () => {
  console.log("Listening to port "+ process.env.PORT);
});
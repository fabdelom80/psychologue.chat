// define constructor function that gets `io` send to it

module.exports = function (io) {
    let queued_clients = [];
    let queued_praticiens = [];


    io.on('connection', (socket) => {
        socket.on('user_start', (user) => {
            // check doublon adress mac ou autre pour unicité

            if (queued_clients.length == 0 && queued_praticiens.length > 0) {
                let praticien = queued_praticiens.shift();
                while(praticien.socket.disconnected && queued_praticiens.length > 0){
                    praticien = queued_praticiens.shift();
                }
                if(praticien.socket.disconnected){
                    queued_clients.push({ id: socket.id, socket: socket });
                } else {
                    io.to(user.id).emit('room_found', praticien.roomId);
                    io.to(praticien.id).emit('room_found', praticien.roomId);
                }
            } else {
                queued_clients.push({ id: socket.id, socket: socket });
            }
        });


        socket.on('praticien_start', (praticien) => {
            if (queued_praticiens.length == 0 && queued_clients.length > 0) {
                let user = queued_clients.shift()
                while(user.socket.disconnected && queued_clients.length > 0){
                    user = queued_clients.shift();
                }
                if(user.socket.disconnected){
                    queued_praticiens.push({ id: socket.id, socket: socket });
                } else {
                    io.to(praticien.id).emit('room_found', praticien.roomId);
                    io.to(user.id).emit('room_found', praticien.roomId);
                }
            } else {
                queued_praticiens.push({ id: socket.id, socket: socket, roomId: praticien.roomId });
            }
        });

        socket.on('room_join_request', payload => {
            socket.join(payload.roomId)
            var room = io.sockets.adapter.rooms.get(payload.roomId);
            if (room.size <= 2) {
                socket.joinedRoom = payload.roomId;
                socket.to(socket.joinedRoom).emit('room_users', socket.id);
            } else {
                socket.leave(payload.roomId);
            }
        })

        socket.on('offer_signal', payload => {
            io.to(payload.calleeId).emit('offer', { signalData: payload.signalData, callerId: socket.id });
        });

        socket.on('answer_signal', payload => {
            io.to(payload.callerId).emit('answer', { signalData: payload.signalData, calleeId: socket.id });
        });

        socket.on('user_ask_visio', payload => {
            socket.to(socket.joinedRoom).emit('user_ask_pra',payload);
        });

        socket.on('pra_answer_visio', payload => {
            socket.to(socket.joinedRoom).emit('pra_answer_user',payload);
        });

        socket.on('disconnect', (reason) => {
            if (socket.joinedRoom != null) {
                socket.to(socket.joinedRoom).emit('room_left', { type: 'disconnected' })
                socket.leave(socket.joinedRoom);
            }
        })
    });
};